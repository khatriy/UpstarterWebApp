<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test',function(){
    $data=[];
    $data['name']='yash';
    Mail::send('mails.testMail',$data,function ($message){
    $message->from('yashkhatri.001@gmail.com','Yash Khatri');
    $message->to('yashkhatri.001@gmail.com');
    });
});
Route::get('/companies',['as'=>'companies','uses'=>'CompanyController@getCompanies']);
Route::get('/dashboard/checkAuth','DashboardController@getUser')->name('getUserType')->middleware('auth');
Route::get('dashboard/company/getProfile','DashboardController@getCompanyProfile')->middleware('auth')->name('getCompanyProfile');
Route::get('company/Dashboard/getNotifications','CompanyController@getNotifications')->name('getNotifications')->middleware('auth');
Route::get('dashboard/company/getAddedJobs','CompanyController@getAddedJobs')->name('getAddedJobs')->middleware('auth');
Route::get('company/Dashboard/getUpcomingEvents','CompanyController@getUpcomingEvents')->name('getUpcomingEvents')->middleware('auth');
Route::post('company/Dashboard/addNewEventAttendee','CompanyController@addNewAttendee')->name('addNewAttendee')->middleware('auth');
Route::get('company/Dashboard/getEventsCompaniesRegisteredFor','CompanyController@getEventsCompaniesRegisteredFor')->name('EventsCompaniesRegisteredFor')->middleware('auth');
Auth::routes();

Route::get('/dashboard', 'DashboardController@getDashboard')->name('dashboard')->middleware('auth');
Route::get('/testNotify', 'DashboardController@testNotify')->name('testNotify');


//Admin Routes
Route::get('admin/Dashboard/getNotifications','AdminController@getNotifications')->name('getNotifications')->middleware('auth');
Route::put('admin/Dashboard/authorizeCompany','AdminController@authorizeCompany')->name('authorizeCompany')->middleware('auth');
Route::get('admin/Dashboard/getRegCompanies', 'AdminController@getRegCompanies')->name('getRegCompanies')->middleware('auth');
Route::put('admin/Dashboard/cmpStatusUpdate','AdminController@cmpStatusUpdate')->name('cmpStatusUpdate')->middleware('auth');
Route::get('admin/Dashboard/getApprovedCompanies','AdminController@getApprovedCompanies')->name('getApprovedCompanies')->middleware('auth');
Route::get('admin/Dashboard/getDeclinedCompanies','AdminController@getDeclinedCompanies')->name('getDeclinedCompanies')->middleware('auth');
Route::post('admin/Dashboard/addEvent','AdminController@addEvent')->name('addEvent')->middleware('auth');
Route::get('admin/Dashboard/getEvents','AdminController@getEvents')->name('getEvents')->middleware('auth');
Route::delete('admin/Dashboard/deleteEvent','AdminController@deleteEvent')->name('deleteEvent')->middleware('auth');
Route::put('admin/Dashboard/updateEvent','AdminController@updateEvent')->name('updateEvent')->middleware('auth');
Route::get('admin/Dashboard/getRegisteredUsers','AdminController@getRegisteredUsers')->name('getRegisteredUsers')->middleware('auth');
Route::get('admin/Dashboard/getRegisteredCompanies','AdminController@getRegisteredCompanies')->name('getRegisteredCompanies')->middleware('auth');
//Admin Routes End getDeclinedCompanies


//Post requests
Route::post('/dashboard/company/submitProfileData','DashboardController@submitProfile')->middleware('auth');
Route::post('company/Dashboard/addNewJob','CompanyController@addNewJob')->name('addNewJob')->middleware('auth');
Route::put('company/Dashboard/updateJob','CompanyController@updateJob')->name('updateJob')->middleware('auth');
Route::delete('company/Dashboard/deleteJob','CompanyController@deleteJob')->name('deleteJob')->middleware('auth');


Route::get('/hubSpotTest/dataJson',function(){
   return \App\User::all()->jsonSerialize();
});
Route::post('hubSpotTest/dataJsonPost',function (Request $request){

   return 'hello';
});