var webpack = require('webpack');
module.exports = {

    entry: {
        dashMain:".\\public\\js\\dashboard\\src\\dashBoardMain.js"
    },


    output: {
        path: __dirname+"/public/js/dashboard/dist",
        filename: '[name]Build.js'
    },

    watch: true,
    module:
        {
            loaders: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: 'babel-loader',
                    query: {
                        presets:[     ["es2015", { "loose": true }]
                        ]
                    }
                },
                {
                    test: /\.vue$/,
                    loader: 'vue'
                }
            ]
        },
    plugins: [
        // ...
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"development"'
            }
        })
    ]

};