import RegisteredCompList from "../AdminDash/subComponents/RegisteredCompList";

export const store =new Vuex.Store({
   state:{
       test:'yash',
       user:{},
       waitingModalShow:false,

       companyProfileData:{},
       CompanyNotifications:{},
       companyDetails:{
           regCompanies:[],
           approvedCompanies:[],
           DeclinedCompanies:[]
           },
       comDetailModal:false,
       comSingleDetail:{},
       CompanyAddedJobs:{},
       CompanyUpdateProfileFormView:false,
       CompanyUpdateProfileFormViewJob:{},
       UpcomingEventForCompanyDash:{},
       notificationDetailModalShow:false,
       upComingEventDetailModalShow:false,
       EventsCompanyRegisteredFor:{},



       AdminNotifications:{},
       adminAddedEvents:{},
       editEventModalView:false,
       editEventModalData:{},
       RegisteredUsers:{},
       RegisteredCompanies:{},
       UserListAdminHomeShow:false,
       CompanyListAdminHomeShow:false,
       EventsListAdminHomeShow:false

   } ,
    mutations:{
       mutateCompanyRegisteredEvents(state,payload){
           state.EventsCompanyRegisteredFor=payload;
       },
       mutateUpComingEventDetailModalShow(state,payload){
           state.upComingEventDetailModalShow=payload;
       },
       mutateNotificationDetailModalShow(state,payload){
         state.notificationDetailModalShow=payload;
       },
        mutateUpcomingEventForCompanyDash(state,payload){
            state.UpcomingEventForCompanyDash=payload;
        },
        mutateUserListAdminHomeShow(state){
            state.EventsListAdminHomeShow=false;
            state.CompanyListAdminHomeShow=false;
            state.UserListAdminHomeShow=!state.UserListAdminHomeShow;
        },
        mutateCompanyListAdminHomeShow(state){
            state.UserListAdminHomeShow=false;
            state.EventsListAdminHomeShow=false;
            state.CompanyListAdminHomeShow=!state.CompanyListAdminHomeShow;
        },
        mutateEventListAdminHomeShow(state){
            state.UserListAdminHomeShow=false;
            state.CompanyListAdminHomeShow=false;
            state.EventsListAdminHomeShow=!state.EventsListAdminHomeShow;
        },
        mutateRegisteredCompanies(state,payload){
            state.RegisteredCompanies=payload;
        },
        mutateRegisteredUsers(state,payload){
            state.RegisteredUsers=payload;
        },
       mutateEditEventModalView(state,data){
           state.editEventModalView=data.View;
           state.editEventModalData=data.data;
       },
       mutateCompanyUpdateForm(state,data){
           state.CompanyUpdateProfileFormView=data.View;
           state.CompanyUpdateProfileFormViewJob=data.Job;
       },
        mutateCompanyAddedJobs(state,payload){
            state.CompanyAddedJobs=payload;
        },
        mutateComDetailModal(state,payload){
            state.comDetailModal=payload;
        },
        mutateComSingleDetail(state,comSingleDetail){

            state.comSingleDetail=comSingleDetail;
        },
        mutateRegCompaniesList(state,payload){
            console.log(payload);
             state.companyDetails.regCompanies.splice(payload,1);
        },
        mutateApprovedCompaniesList(state,payload){
            console.log(payload);
            state.companyDetails.approvedCompanies.splice(payload,1);

        },
        mutateDeclinedCompaniesList(state,payload){
            console.log(payload);
            state.companyDetails.DeclinedCompanies.splice(payload,1);

        },
        mutateAdminRegCompanies(state,payload){
            state.companyDetails.regCompanies=payload;
        },
        mutateAdminApprovedCompanies(state,payload){
            state.companyDetails.approvedCompanies=payload;
        },
        mutateAdminDeclinedCompanies(state,payload){
            state.companyDetails.DeclinedCompanies=payload;
        },
        mutateCompanyNotifications(state,payload){
            state.CompanyNotifications=payload;
        },
        mutateWaitingModalShow(state,payload){
          state.waitingModalShow=payload;
        },
        mutateAdminNotifications(state,payload){
            state.AdminNotifications=payload;
        },
            mutateUser(state,payload){
                state.user=payload;
            },
        mutateCompanyProfile(state,payload){
                state.companyProfileData=payload;
        },
        mutateAdminEvents(state,payload){
            state.adminAddedEvents=payload;
        }
    },
    actions:{
       /*
       AdminDashActionsStart
        */
        getRegisteredCompaniesAdmin({commit,state}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "admin/Dashboard/getRegisteredCompanies",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        console.log(result);
                        commit('mutateRegisteredCompanies',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        getRegisteredUsers({commit,state}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "admin/Dashboard/getRegisteredUsers",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        console.log(result);
                        commit('mutateRegisteredUsers',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        getDeclinedCompanies({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "admin/Dashboard/getDeclinedCompanies",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        console.log(result);
                        commit('mutateAdminDeclinedCompanies',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        getApprovedCompanies({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "admin/Dashboard/getApprovedCompanies",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        console.log(result);
                        commit('mutateAdminApprovedCompanies',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        updateCmpStatus({state,commit},data){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'admin/Dashboard/cmpStatusUpdate',
                    type:"PUT",
                    data:data,
                    dataType:"JSON",
                    cache: false,
                    success: function (result) {
                        console.log('Success Promise');
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error in Promise');
                        reject(error);
                    }

                });
            })
        },
        getRegisteredCompanies({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "admin/Dashboard/getRegCompanies",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        console.log(result);
                        commit('mutateAdminRegCompanies',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        authorizeCompanyByAdmin({commit,state},data){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "PUT",
                    data:data,
                    url: "admin/Dashboard/authorizeCompany",
                    dataType:"JSON",
                    cache: false,
                    success: function (result) {
                        console.log('Success Promise');
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error in Promise');
                        reject(error);
                    }

                });

            });
        },
        getAdminNotifications({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "admin/Dashboard/getNotifications",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        console.log(result);
                        commit('mutateAdminNotifications',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })


        },
        insertEventDetails({commit,state},data){
            return new Promise((resolve,reject)=>{
                commit('mutateWaitingModalShow',true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'admin/Dashboard/addEvent',
                    type:"POST",
                    data:data,
                    dataType:"JSON",
                    cache: false,
                    success: function (result) {
                        console.log('Success Promise');
                        resolve(result);
                        commit('mutateWaitingModalShow',false);
                    },
                    error: function (error) {
                        console.log('error in Promise');
                        reject(error);
                    }

                });
            })


        },
        getAllEvents({commit,state}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "admin/Dashboard/getEvents",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        console.log(result);
                        commit('mutateAdminEvents',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        updateEventDetails({commit,state},data){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=> {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'admin/Dashboard/updateEvent',
                    type:"PUT",
                    data:data,
                    dataType:"JSON",
                    cache: false,
                    success: function (result) {
                        console.log('Success Promise');
                        commit('mutateAdminEvents', result);
                        commit('mutateWaitingModalShow', false);
                        resolve(result);

                    },
                    error: function (error) {
                        console.log('error in Promise');
                        reject(error);
                    }

                });
            });
        },
        deleteEvent({commit,state},data){
            console.log('in delete');
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=> {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'admin/Dashboard/deleteEvent',
                    type:"DELETE",
                    data:data,
                    dataType:"JSON",
                    cache: false,
                    success: function (result) {
                        console.log('Success Promise');
                        commit('mutateAdminEvents', result);
                        commit('mutateWaitingModalShow', false);
                        resolve(result);

                    },
                    error: function (error) {
                        console.log('error in Promise');
                        reject(error);
                    }

                });
            });
        },
        /*
       AdminDashActionsEnd
        */


        getUserType({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "dashboard/checkAuth",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        commit('mutateUser',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        checkProfileDetails({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "dashboard/company/getProfile",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        commit('mutateCompanyProfile',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            });
        },
        insertFromData({state,commit},profileData){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    data:profileData,
                    url: "dashboard/company/submitProfileData",
                    dataType: "JSON",
                    contentType:false,
                    cache: false,
                    processData:false,
                    success: function (result) {
                        commit('mutateCompanyProfile',result);
                        commit('mutateWaitingModalShow',false);
                        console.log('form submit');
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });

            });
        },
        getCompanyNotifications({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "company/Dashboard/getNotifications",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        commit('mutateCompanyNotifications',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        insertJobDetails({state,commit},data){
            commit('mutateWaitingModalShow',true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'company/Dashboard/addNewJob',
                type:"POST",
                data:data,
                dataType:"JSON",
                cache: false,
                success: function (result) {
                    console.log('Success Promise');
                        resolve(result);
                        commit('mutateWaitingModalShow',false);

                },
                error: function (error) {
                    console.log('error in Promise');
                    reject(error);
                }

            });
        },
        getAvailableJobs({state,commit}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=> {
                $.ajax({
                    type: "GET",
                    url: "dashboard/company/getAddedJobs",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        commit('mutateCompanyAddedJobs', result);
                        commit('mutateWaitingModalShow', false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            });
        },
        updateJobDetails({state,commit},data){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=> {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'company/Dashboard/updateJob',
                type:"PUT",
                data:data,
                dataType:"JSON",
                cache: false,
                success: function (result) {
                    console.log('Success Promise');
                    commit('mutateCompanyAddedJobs', result);
                    commit('mutateWaitingModalShow', false);
                    resolve(result);

                },
                error: function (error) {
                    console.log('error in Promise');
                    reject(error);
                }

            });
        });

        },
        deleteJob({state,commit},data){
            console.log('in delete');
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=> {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'company/Dashboard/deleteJob',
                    type:"DELETE",
                    data:data,
                    dataType:"JSON",
                    cache: false,
                    success: function (result) {
                        console.log('Success Promise');
                        commit('mutateCompanyAddedJobs', result);
                        commit('mutateWaitingModalShow', false);
                        resolve(result);

                    },
                    error: function (error) {
                        console.log('error in Promise');
                        reject(error);
                    }

                });
            });
        },
        getAllUpcomingEvents({commit,state}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "company/Dashboard/getUpcomingEvents",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        commit('mutateUpcomingEventForCompanyDash',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        },
        createAPlaceForCompany({commit,state},data){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'company/Dashboard/addNewEventAttendee',
                    type:"POST",
                    data:data,
                    dataType:"JSON",
                    cache: false,
                    success: function (result) {
                        console.log('Success Promise');
                        commit('mutateCompanyRegisteredEvents',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error in Promise');
                        reject(error);
                    }

                });
            })

        },
        getAllEventsCompaniesRegisteredFor({commit,state}){
            commit('mutateWaitingModalShow',true);
            return new Promise((resolve,reject)=>{
                $.ajax({
                    type: "GET",
                    url: "company/Dashboard/getEventsCompaniesRegisteredFor",
                    dataType: "JSON",
                    cache: false,
                    success: function (result) {
                        commit('mutateCompanyRegisteredEvents',result);
                        commit('mutateWaitingModalShow',false);
                        resolve(result);
                    },
                    error: function (error) {
                        console.log('error');
                        reject(error);
                    }

                });
            })
        }
    }
});