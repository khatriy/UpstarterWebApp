import adminDashMain from './AdminDash/adminDashMain.js';
import CompanyDashMain from './CompanyDash/CompanyDashMain.js';
import UserDashMain from './UserDash/UserDashMain.js';
import {store} from './vuex/store.js'
import waitingModal from './Shared/waitingModal.js';
import VeeValidate from 'vee-validate';
Vue.config.devtools = true;
var DashBoard=new Vue({
    store,
   el:'#dashBoardMain',
    data(){
      return {

      }
    },
    computed:{
        waitingModalShow(){
            return this.$store.state.waitingModalShow;
        },
      adminDashView(){
         if(Object.keys(store.state.user).length!==0){
            let userType=store.state.user.userType;
            if(userType===2){
               return true;
            }
            else return false;
         }
         else return false;
      },
      companyDashView(){
          if(Object.keys(store.state.user).length!==0){
              let userType=store.state.user.userType;
              if(userType===1){
                  return true;
              }
              else return false;
          }
          else return false;
      },
        userDashView(){
            if(Object.keys(store.state.user).length!==0){
                let userType=store.state.user.userType;
                if(userType==0){
                    return true;
                }
                else return false;
            }
            else return false;
        }
    },
    created(){
      var promise=store.dispatch('getUserType');
      promise.then((success)=>{
         console.log(success);
      },(error)=>{
   console.log(error)
      });
    },
    components:{adminDashMain,CompanyDashMain,UserDashMain,waitingModal},
   template:`<div>
<adminDashMain v-if="adminDashView"></adminDashMain>
<CompanyDashMain v-if="companyDashView"></CompanyDashMain>
<UserDashMain v-if="userDashView"></UserDashMain>
<waitingModal v-if="waitingModalShow"></waitingModal>
</div>`
});