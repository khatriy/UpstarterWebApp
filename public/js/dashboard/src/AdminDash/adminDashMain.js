import headerComp from '../AdminDash/subComponents/header.js';
import router from "../AdminDash/router/router.js";
import {store} from "../vuex/store";
import companyDetailModal from './subComponents/companyDetailModal.js';

export default({
    store,
    router,
   name:'adminDashMain',
    data(){
        return{
            numOfNotifications:0
        }
    },
    computed:{
        companyDetailModalShow(){
            return this.$store.state.comDetailModal;
        },
        companyDetail(){
            return this.$store.state.comSingleDetail;
        }
    },
    created(){
      let promise1=this.$store.dispatch('getAdminNotifications');
      promise1.then((notifications)=>{
          this.numOfNotifications=notifications.length;
      },(reject)=>{
console.log('There is a error in adminDashMain created ajax request');
      });
        let promise2=this.$store.dispatch('getRegisteredUsers');
        promise2.then((success)=>{
            console.log('get register promise success with '+success);
        },(error)=>{
            console.log('get register promise error with '+error);
        })
        var promise3=this.$store.dispatch('getAllEvents');
        promise3.then((success)=>{
            this.events=success;
            console.log(success);
        },(error)=>{
            console.log(error);
        })
        let promise4=this.$store.dispatch('getRegisteredCompaniesAdmin');
        promise4.then((success)=>{
            console.log('getRegistered Companies promise success with'+success);
        },(error)=>{
            console.log('getRegistered Companies promise failed with '+error)
        });
    },
    components:{headerComp,companyDetailModal},
   template:`<div>
<headerComp :numOfNotifications="numOfNotifications"></headerComp>

<div class="DashBoardContentNew"><router-view></router-view></div>
<companyDetailModal v-if="companyDetailModalShow" :companyDetail="companyDetail"></companyDetailModal></div>`
});