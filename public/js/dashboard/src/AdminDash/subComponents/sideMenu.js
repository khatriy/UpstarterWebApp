export default ({
    name:'sideMenuComp',
    props:['numOfNotifications'],
    data(){
      return {
          toggleCompany:false,
          activeClass:'fa fa-arrow-down',
          notActiveClass:'fa arrow',
          toggleEvent:false
      }
    },
    template:`<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav in" id="side-menu">
                        <li>
                            <router-link to="/"><i class="fa fa-dashboard fa-fw"></i>Dashboard</router-link>
                        </li>
                        <li>
                            <router-link to="/Notifications" ><i class="fa fa-globe fa-fw"></i>Notifications<span class="NotificationPanel">{{numOfNotifications}}</span></router-link>
                        </li>
                       
                         <li style="cursor: pointer">
                            <a @click="toggleCompanyClass()"><i class="fa fa-building fa-fw"></i> Companies<span style="float: right" :class="[toggleCompany ? activeClass: notActiveClass]"></span></a>
                            <ul class="nav nav-second-level collapse in" v-bind:class="{in:toggleCompany}" aria-expanded="false" style="height: 0px;">
                                <li>
                                   <router-link to="/RegCompanies">Registered</router-link>
                                </li>
                                <li>
                                    <router-link to="/ApprovedCompanies">Approved</router-link>
                                </li>
                                <li>
                                   <router-link to="/DeclinedCompanies">Declined</router-link>
                                </li>
            
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li style="cursor: pointer; margin-top: 110px;">
                            <a @click="toggleEventClass()"><i class="fa fa-building fa-fw"></i> Events<span style="float: right" :class="[toggleEvent ? activeClass: notActiveClass]" ></span></a>
                            <ul class="nav nav-second-level collapse" v-bind:class="{in:toggleEvent}" aria-expanded="false" style="height: 0px;">
                                <li>
                                   <router-link to="/AddEvent">Add New Event</router-link>
                                </li>
                                <li>
                                    <router-link to="/ViewEvents">View Events</router-link>
                                </li>
                            
            
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>          
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->`,
    methods:{
        toggleCompanyClass(){
            this.toggleCompany=!this.toggleCompany;
        },
        toggleEventClass(){
            this.toggleEvent=!this.toggleEvent;
        }

    }
});