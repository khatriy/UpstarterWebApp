import RegisteredCompanyTemplate from './RegisterdCompanyTemplate.js';
export default ({
   name:'DeclinedCompanies',
    components:{RegisteredCompanyTemplate},
    created(){
        let Promise=this.$store.dispatch('getDeclinedCompanies');
        Promise.then((success)=>{
            console.log('get Approved companies promise with success');
            console.log(success);

        },(error)=>{
            console.log('get Reg companies promise with error')
            console.log(error);
        })
    },computed:{
        DeclinedCompanies(){
            return this.$store.state.companyDetails.DeclinedCompanies;
        }
    },
    template:`<div class="panel panel-default DashBoardContent" >
<div class="panel-heading">Registered Companies </div>
<div class="panel-body">
<RegisteredCompanyTemplate v-for="(DeclinedCompany,index) in DeclinedCompanies" :type="2" :companyData="DeclinedCompany" :keyValue="index"></RegisteredCompanyTemplate>
</div>
</div>`

});