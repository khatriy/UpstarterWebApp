import NewRegRequestNotifyComp from './subComps/NewRegRequesNotifyComp';
import EventRegisterNotificationsByCompany from './subComps/EventRegisterNotificationsByCompany';
export default ({
   name:'notificationsComponent',
    components:{
        NewRegRequestNotifyComp,EventRegisterNotificationsByCompany
    },
    computed:{
      notifications(){
         return this.$store.state.AdminNotifications;
      },
        NewRegRequestNotifications(){
          let notifications=this.$store.state.AdminNotifications;
          let filterNotifyArray=[];
          for(let i=0;i<Object.keys(notifications).length;i++){
              if(notifications[i].type==="App\\Notifications\\newCompanyRegistration"){
                  filterNotifyArray.push(notifications[i]);
              }
          }
          return filterNotifyArray;
        },
        EventRegisterNotifications(){
            let notifications=this.$store.state.AdminNotifications;
            let filterNotifyArray=[];
            for(let i=0;i<Object.keys(notifications).length;i++){
                if(notifications[i].type==="App\\Notifications\\PostEventRegisterNotificationToAdmin"){
                    filterNotifyArray.push(notifications[i]);
                }
            }
            return filterNotifyArray;
        }
    },
   template:`<div>
               <NewRegRequestNotifyComp :notifications="NewRegRequestNotifications"></NewRegRequestNotifyComp>
               <EventRegisterNotificationsByCompany :notifications="EventRegisterNotifications"></EventRegisterNotificationsByCompany>
                
            </div>`,
    methods:{
      accept(cmpId,userId,notificationId){
         console.log('accept '+cmpId+' '+userId);
          let data={};
          data['cmpId']=cmpId;
          data['userId']=userId;
          data['notId']=notificationId;
         var Promise=this.$store.dispatch('authorizeCompanyByAdmin',data);
         Promise.then((success)=>{
             document.location.reload();
         },(error)=>{
console.log("error in the notification components's accept function");
         })
      }
    }
});