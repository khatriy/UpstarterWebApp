import moment from 'moment'
Vue.prototype.moment = moment;
export default ({
   name:'EventRegisterNotificationsByCompany',
    props:['notifications'],
   template:`
    <div class="col-sm-4" style="padding-top: 2%">
        <div class="panel panel-default">
            <div class="panel-heading">
            <button @click="hidePanel($event)" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               Event Registration Notifications
            </div>
            <!-- /.panel-heading -->
            <div v-for="notification in notifications" class="panel-body" style="padding: 5px">
               <div class="alert alert-success alert-dismissable" style="margin-bottom: 0px">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" @click="markAsRead(notification)">×</button>
                                <span style="word-spacing: 5px; font-size: 18px"><a style="cursor:pointer;">{{getCompanyName(notification)}}</a> register for <a style="cursor: pointer">{{getEventName(notification)}}</a></span >
                                   <div style="color: black; font-size: 18px; word-spacing: 5px">Stand Choice:
                                    {{notification.data.companyEventDetails.standChoice}}</div> 
                                  <div style="color: black">
                                    {{timeDiffComputed(notification.created_at)}}</div> 
               </div>
                           
            </div>
            <!-- .panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    `,
    methods:{
        timeDiffComputed(createdTime){
            let now=moment().format('YYYY-MM-DD HH:mm:ss'); let then=createdTime;
            return moment.duration(moment(now,'YYYY-MM-DD HH:mm:ss').diff(moment(then,'YYYY-MM-DD HH:mm:ss'))).humanize()+' ago'
        },
        getCompanyName(notification){
            let companyDetails=notification.data.companyDetails;
            let companyName=companyDetails.cmpName;
            return companyName;
        },
        getEventName(notification){
            let companyAtEventDetails=notification.data.companyEventDetails;
            let eventName=companyAtEventDetails.eventName;
            return eventName;

        },
        markAsRead(notification){

        }
    }
});