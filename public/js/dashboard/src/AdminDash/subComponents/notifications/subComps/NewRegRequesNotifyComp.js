export default ({
   name:'NewRegRequestNotifyComp',
    props:['notifications'],
    template:`<div class="col-sm-6" style="padding-top: 2%">
                <div class=" panel panel-default" style="margin-bottom:0px">
                    <div class="panel-heading">New Registration Requests</div>
                    <div class="panel panel-body" style="padding: 0px">
                        
                  <div v-for="notification in notifications"  class="card  fadeIn Notify">
                    <div class="card-image col-md-3" style="overflow: hidden">
                        <img :src="getImgSource(notification)" width="100%" height="100%" style="padding: 5%">
                    </div>
                    <div class="card-block col-md-9">
                        <h4 class="card-title">{{notification.data.cmpName}}</h4>
                        <h6 class="card-subtitle mb-2 text-muted">{{notification.data.cmpAddress}}</h6>
                        <div style="overflow: hidden">
                            <p style="float: left;color: orange; display: inline"><span style="padding: 10%; display: inline" class="fa fa-envelope"></span>{{notification.data.cmpEmail}}</p>
                            <p style="color:orange; margin-left: 70%"><span style="padding: 10%; display: inline" class="fa fa-phone-square"></span>{{notification.data.telephone}}</p>
                        </div>
                        <div>
                            <a style="display: inline"  class="btn btn-success" @click="accept(notification.data.cmpId,notification.data.userId,notification.id)">Accept<span style="padding: 2%" class="fa fa-check"></span></a>
                            <a style="display: inline"  class="btn btn-primary"@click="decline(notification.data.cmpId)">Decline<Span style="padding: 2%"  class="fa fa-remove"></Span></a>
                        </div>
                    </div>
                </div>
                      
                  </div>
                </div> 
               </div>
                `,
    methods:{
       getImgSource(notification){
           return 'storage/'+notification.data.cmpLogo
       }
    }
});