export default ({
   name:'AddNewEvent',
    data(){
        return {
            EventDetails:{
                EventTitle:'',
                EventDescription:'',
                EventTime:'',
                EventDate:'',
                EventLocation:''
            }
        }
    },
    computed:{
        formSubmit(){
            if(!this.errors.any())
            {
                var checkStatus=true;
                $.each(this.EventDetails,(key,value)=>{
                    if(value===''){
                        checkStatus=false;
                        return false;
                    }
                });
                return checkStatus;
            }

            else return false;
        }
    },
    template:`<div id="page-wrapper" style="min-height: 625px;" class="col-sm-8">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add new Event</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Fill the Detail
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <form  @submit.prevent="validateBeforeSubmit()" role="form">
                                <div class="form-group">
                                    <label>Event Title</label>
                                    <input v-model="EventDetails.EventTitle" class="form-control"  name="EventTitle" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('EventTitle')">{{errors.first('EventTitle')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Event Date</label>
                                    <input v-model="EventDetails.EventDate" class="form-control"  name="EventDate" v-validate="'required'" type="date" >
                                    <p class="text-danger" v-if="errors.has('EventDate')">{{errors.first('EventDate')}}</p>
                                </div>
                                <div class="form-group">
                                            <label>Event Time</label>
                                    <input v-model="EventDetails.EventTime" class="form-control"  name="EventTime" v-validate="'required|time'" type="time" >
                                     <p class="text-danger" v-if="errors.has('EventTime')">{{errors.first('EventTime')}}</p>

                                        </div>
                                 <div class="form-group">
                                    <label>Event Location</label>
                                    <textarea v-model="EventDetails.EventLocation" class="form-control"  name="EventLocation" v-validate="'required'" type="text" ></textarea>
                                    <p class="text-danger" v-if="errors.has('EventLocation')">{{errors.first('EventLocation')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Event Description</label>
                                    <textarea v-model="EventDetails.EventDescription" class="form-control"  name="EventDescription" v-validate="'required'" type="text" ></textarea>
                                    <p class="text-danger" v-if="errors.has('EventDescription')">{{errors.first('EventDescription')}}</p>
                                </div>
             
                                <button v-if="formSubmit" type="submit" class="btn btn-default">Submit Button</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`,
    methods:{
        validateBeforeSubmit(){
            if(this.formSubmit)
            {
                let formData={};
                formData['eventTitle']=this.EventDetails.EventTitle;
                formData['eventDate']=this.EventDetails.EventDate;
                formData['eventDescription']=this.EventDetails.EventDescription;
                formData['eventTime']=this.EventDetails.EventTime;
                formData['eventLocation']=this.EventDetails.EventLocation;
                let promise=this.$store.dispatch('insertEventDetails',formData);
                promise.then((success)=>{
                    this.$router.push('/ViewEvents')
                },(error)=>{
                    console.log(error)
                })
            }
            else console.log('please fill the form correctly');

        }
    }
});