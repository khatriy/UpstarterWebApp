
export default ({
    name:'eventComp',
    data(){
        return {

        }
    },
    props:['eventBind','indexBind'],
    template:`           <tr>

                                            <td>{{indexBind+1}}</td>
                                            <td>{{eventBind.Title}}</td>
                                            <td>{{eventBind.Description}}</td>
                                            <td>{{eventBind.location}}</td>
                                            <td>{{eventBind.Time}},{{eventBind.Date}}</td>                                   
                                            <td>
                                                <div class="btn btn-group">
                                                <button @click="updateEvents(eventBind)" class="btn btn-primary" style="border-radius:10px">update</button>
                                                <div class="col-sm-2"></div>
                                                <button @click="deleteEvents(eventBind)" style="border-radius: 10px" class="btn btn-danger">delete</button>
                                                </div>
                                            </td>
  </tr>
  

    `
    ,
    methods: {
        updateEvents(event) {
            let payload={};
            payload['data']=event;
            payload['View']=true;
            this.$store.commit('mutateEditEventModalView',payload);

        },
        deleteEvents(eventBind) {
            console.log('delete job');
            let formData = {};
            formData['eventId'] = eventBind.eventId;
            let promise = this.$store.dispatch('deleteEvent', formData);
            promise.then((success) => {
                console.log(success);
            }, (error) => {
                console.log(error);
            })

        }
    }
});