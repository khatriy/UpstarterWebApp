import eventComp from './singleEventComp';
import eventUpdateFormComp from './eventUpdateFormModal'
export default ({
    name:'ViewEventsComp',
    components:{eventComp,eventUpdateFormComp},
    data(){
      return {
      }
    },
    created(){
        var promise3=this.$store.dispatch('getAllEvents');
        promise3.then((success)=>{
            this.events=success;
            console.log(success);
        },(error)=>{
            console.log(error);
        })

    },
    computed:{
      numOfEvents(){
          let events=this.$store.state.adminAddedEvents
          return Object.keys(events).length;
      },
        events(){
            return this.$store.state.adminAddedEvents;
        },
        editEventModalView(){
            return this.$store.state.editEventModalView;
        }
    },
    template:`<div>
     <div v-if="numOfEvents" class="DashBoardContent" style="padding: 2%">
<div class="panel panel-default">
                        <div class="panel-heading">
                            Added Events
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Location</th>
                                            <th>Date & Time</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <eventComp v-for="(event,index) in events" :eventBind="event" :indexBind="index"></eventComp>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>          
</div>
<div v-if="!numOfEvents" class="panel panel-default DashBoardContent" style="padding: 2%" >
                        <div class="panel-heading">
                            Alert Styles
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="alert alert-success">
                                No Events Added Yet!! Click to <router-link to="AddEvent" class="alert-link">Add Event</router-link>.
                            </div>
                           
                        </div>
                        <!-- .panel-body -->
                    </div>
<eventUpdateFormComp v-if="editEventModalView"></eventUpdateFormComp>                   
</div>`
})