import RegisteredCompanyTemplate from './RegisterdCompanyTemplate.js';
export default ({
    name:'RegisteredCompanyList',
    components:{RegisteredCompanyTemplate},
    data(){
        return{
        }
    },
    created(){
      let Promise=this.$store.dispatch('getRegisteredCompanies');
      Promise.then((success)=>{
          console.log('get Reg comapnies promise with success');
          console.log(success);

      },(error)=>{
          console.log('get Reg companies promise with error')
          console.log(error);
      })
    },computed:{
        RegCompanies(){
            return this.$store.state.companyDetails.regCompanies;
        }
    },
    template:`<div class="panel panel-default DashBoardContent" >
<div class="panel-heading">Registered Companies </div>
<div class="panel-body">
<RegisteredCompanyTemplate v-for="(RegCompany,index) in RegCompanies" :type="0" :companyData="RegCompany" :keyValue="index"></RegisteredCompanyTemplate>
</div>
</div>`
})