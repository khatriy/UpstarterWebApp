export default ({
    name:'RegisteredCompanyTemplate',
    props:['companyData','keyValue','type'],
    template:`<div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading" >
                           <a style="color: white; font-weight: bold; cursor: pointer" type="button" @click="showCompDetailModal(companyData)">{{companyData.cmpName}}</a>
                        </div>
                        <div class="panel-body">
                            <p>Email: {{companyData.cmpEmail}}</p>
                            <p>Telephone: {{companyData.telephone}}</p>
                        </div>
                        <div class="panel-footer row">
                            <button @click="adminAction(companyData.cmpId,companyData.cmpStatus,keyValue)" class="btn btn-success primary col-lg-4" :class="{disabled:companyData.cmpStatus}">Approve</button>
                            <div class="col-sm-2"></div>
                            <div @click="adminAction(companyData.cmpId,companyData.cmpStatus,keyValue)" class="btn btn-danger col-sm-4"  :class="{disabled:!companyData.cmpStatus}">Decline</div>
                        </div>
                    </div>
                </div>`,
    methods:{
        showCompDetailModal(comDetailData){
            this.$store.commit('mutateComDetailModal',true);
            this.$store.commit('mutateComSingleDetail',comDetailData);
        },
        adminAction(cmpId,status,key){
            let changedStaus=status?0:1;
            console.log(cmpId+' '+changedStaus+' '+key);
            let formData={}
            formData['cmpId']=cmpId;
            formData['changedStatus']=changedStaus;
            var promise=this.$store.dispatch('updateCmpStatus',formData);
            promise.then((success)=>{
                if(success!==false)
                    if(this.type===0)
                        this.$store.commit('mutateRegCompaniesList',key);
                    else if(this.type===1)
                        this.$store.commit('mutateApprovedCompaniesList',key);
                    else if(this.type===2)
                        this.$store.commit('mutateDeclinedCompaniesList',key);
                this.$store.commit('mutateWaitingModalShow',false);
            },(error)=>{
                console.log("error in company update promise with "+ error);
            });

            //send and ajax request to update the value
        }
    }
})