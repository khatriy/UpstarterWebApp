export default ({
   name:'companyDetailModal',
    props:['companyDetail'],
    data(){
      return{
          i:0
      }
    },
   template:`<div class="modal--main all">

        <div class="modal--background all">

        </div>

        <!--  Client MODAL   -->
        
                    <div class="panel panel-default modal--comp" style="padding: 0%;">
                        <div class="panel-heading">
                            Company Detail<a @click="hideComDetailModal()" style="float: right; cursor: pointer" class="fa fa-lg fa-close"></a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                         
                                            <th>Attribute</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index,key) in companyDetail">
                                           <td>{{key+1}}</td>
                                            <td>{{index}}</td>
                                            <td>{{data}}</td>
                                            
                                        </tr>
                                      
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
        

    `,
    methods:{
        hideComDetailModal(){
            console.log('in function');
            this.$store.commit('mutateComDetailModal',false);
        }

    }
});