export default ({
    name:'mainContent',
    computed:{
        numOfUsers(){
            return Object.keys(this.$store.state.RegisteredUsers).length;
        },
        numOfEvents(){
            return Object.keys(this.$store.state.adminAddedEvents).length;
        },
        numOfCompanies(){
            return Object.keys(this.$store.state.RegisteredCompanies).length;
        }
    },
    template:`<div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{numOfUsers}}</div>
                                    <div>Users</div>
                                </div>
                            </div>
                        </div>
                        <a style="cursor:pointer" @click="userListShow()">
                            <div class="panel-footer">
                               View Details
                                <i class="fa fa-arrow-circle-right"></i>
                              
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-building fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{numOfCompanies}}</div>
                                    <div>Companies</div>
                                </div>
                            </div>
                        </div>
                        <a style="cursor:pointer" @click="companyListShow()">
                            <div class="panel-footer">
                               View Details
                                <i class="fa fa-arrow-circle-right"></i>
                                
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{numOfEvents}}</div>
                                    <div>Events</div>
                                </div>
                            </div>
                        </div>
                        <a style="cursor:pointer" @click="eventsListShow()">
                            <div class="panel-footer">
                               View Details
                                <i class="fa fa-arrow-circle-right"></i>
                                
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">13</div>
                                    <div>Support</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                               View Details
                                <i class="fa fa-arrow-circle-right"></i>
                                
                            </div>
                        </a>
                    </div>
                </div>
            </div>`,
    methods:{
        userListShow(){
            this.$store.commit('mutateUserListAdminHomeShow');
        },
        companyListShow(){
            this.$store.commit('mutateCompanyListAdminHomeShow')
        },
        eventsListShow(){
            this.$store.commit('mutateEventListAdminHomeShow')
        }
    }
});