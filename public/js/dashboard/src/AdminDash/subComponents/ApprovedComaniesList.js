import RegisteredCompanyTemplate from './RegisterdCompanyTemplate.js';
export default ({
   name:'ApprovedCompanies',
    components:{RegisteredCompanyTemplate},
    created(){
        let Promise=this.$store.dispatch('getApprovedCompanies');
        Promise.then((success)=>{
            console.log('get Approved comapnies promise with success');
            console.log(success);

        },(error)=>{
            console.log('get Reg companies promise with error')
            console.log(error);
        })
    },computed:{
        ApprovedCompanies(){
            return this.$store.state.companyDetails.approvedCompanies;
        }
    },
    template:`<div class="panel panel-default DashBoardContent" >
<div class="panel-heading">Registered Companies </div>
<div class="panel-body">
<RegisteredCompanyTemplate v-for="(ApproveCompany,index) in ApprovedCompanies" :type="1" :companyData="ApproveCompany" :keyValue="index"></RegisteredCompanyTemplate>
</div>
</div>`
});