import mainContent from './DashboardHome/mainContent';
import ViewEvents from '../subComponents/Events/ViewEventsComp';
import UsersShow from './DashboardHome/UsersShowComp.js';
import CompanyShow from './DashboardHome/CompanyShowComp.js';
export default ({
   name:'adminDashboardHomeContent',

    data(){
      return {

      }
    },
    computed:{
        EventShow(){
            return this.$store.state.EventsListAdminHomeShow;
        },
        UsersShow(){
            return this.$store.state.UserListAdminHomeShow;
        },
        CompaniesShow(){
            return this.$store.state.CompanyListAdminHomeShow;
        }
    },
    components:{mainContent,ViewEvents,CompanyShow,UsersShow},
    created(){
       //get all the notifications available for admin and store them in the store
    },
   template:`<div style="padding-left: 5px; padding-top: 5px" class="DashBoardContent">
<mainContent></mainContent>
<UsersShow v-if="UsersShow"></UsersShow>
<CompanyShow v-if="CompaniesShow"></CompanyShow>
<ViewEvents v-if="EventShow"></ViewEvents>

</div>`
});