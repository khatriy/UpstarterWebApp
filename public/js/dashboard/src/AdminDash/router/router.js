import adminDashboardHomeContent from '../../AdminDash/subComponents/adminDashboardHomeContent.js';
import notificationsComponent from '../subComponents/notifications/notificationsComponent.js';
import RegisteredCompList from '../../AdminDash/subComponents/RegisteredCompList';
import ApprovedCompanies from '../../AdminDash/subComponents/ApprovedComaniesList.js';
import DeclinedCompanies from '../../AdminDash/subComponents/DeclinedCompaniesList.js'
import AddNewEvent from '../subComponents/Events/AddNewEvent';
import ViewEvents from '../subComponents/Events/ViewEventsComp';
export default new VueRouter({
    routes:[
        {path:'/',name:'home',component:adminDashboardHomeContent},
        {path:'/Notifications',component:notificationsComponent},
        {path:'/RegCompanies',component:RegisteredCompList},
        {path:'/ApprovedCompanies',component:ApprovedCompanies},
        {path:'/DeclinedCompanies',component:DeclinedCompanies},
        {path:'/AddEvent',component:AddNewEvent},
        {path:'/ViewEvents',component:ViewEvents}


        // {path:'/SessionDetail',name:'sessionDetail',component:sessionDefault},
        // { path: '/feed',name:'home',component: feedShowComponent },
        // {
        //     path:'/test',name:'test',component:dummyComponent ApprovedCompanies
        // }
    ]
});