import headerComp from '../CompanyDash/subComponents/header.js';
import router from "../CompanyDash/router/router.js";
import {store} from '../vuex/store.js';

export default ({
    store,
    router,
   name:'CompanyDashMain',
    data(){
      return {
         profileData:{}
      }
    },
    components:{headerComp},
    created(){
        console.log('created');
      var promise=this.$store.dispatch('checkProfileDetails');
      promise.then((success)=>{
         if(Object.keys(success).length!==0){
             console.log(success);
            var obj=this;
            obj.profileData=success;
            router.push({path:'/'})
         }
         else {
            router.push({path:'/profile'})
         }
      },(error)=>{
          console.log(error)
      });
    },
    computed:{

    },
   template:`<div>
<headerComp></headerComp>
<router-view></router-view>

</div>`
});