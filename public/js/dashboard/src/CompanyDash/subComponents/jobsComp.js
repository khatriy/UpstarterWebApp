import updateJobFormComp from './updateJobFromComp';
export default ({
    name:'jobsComp',
    data(){
      return {

      }
    },
    components:{updateJobFormComp},
    created(){
        var promise=this.$store.dispatch('getAvailableJobs');
        promise.then((success)=>{
            console.log(success);
        },(error)=>{
            console.log(error);
        });
    },
    computed:{
      jobs(){
          return this.$store.state.CompanyAddedJobs;
      },
        numOfJobs(){
            let jobs=this.$store.state.CompanyAddedJobs;
            return Object.keys(jobs).length;
        },
        updateFormShow(){
          return this.$store.state.CompanyUpdateProfileFormView;
        },
        job(){
            return this.$store.state.CompanyUpdateProfileFormViewJob;
        }
    },
    template:`
<div>
    <div v-if="numOfJobs" class="DashBoardContentNew" style="padding: 2%">
<div class="panel panel-default">
                        <div class="panel-heading">
                            Active Posted Jobs
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Url</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(job,index) in jobs">
                                            <td>{{index+1}}</td>
                                            <td>{{job.Title}}</td>
                                            <td>{{job.Description}}</td>
                                            <td>{{job.Url}}</td>
                                            <td>
                                                <div class="btn btn-group">
                                                <button @click="updateJob(job)" class="btn btn-primary" style="border-radius:10px">update</button>
                                                <div class="col-sm-2"></div>
                                                <button @click="deleteJobs(job)" style="border-radius: 10px" class="btn btn-danger">delete</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
           
</div>
<updateJobFormComp v-if="updateFormShow" :job="job"></updateJobFormComp>
<div v-if="!numOfJobs" class="panel panel-default DashBoardContent" style="padding: 2%" >
                        <div class="panel-heading">
                            Alert Styles
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="alert alert-success">
                                No jobs Added Yet!! Click to <router-link to="/addJob" class="alert-link">Add Job</router-link>.
                            </div>
                           
                        </div>
                        <!-- .panel-body -->
                    </div>
</div>
    `
    ,
    methods: {
        updateJob(job) {
            var data = {};
            data['View'] = true;
            data['Job'] = job;
            this.$store.commit('mutateCompanyUpdateForm', data);

            console.log('updateJob');
        },
        deleteJobs(job) {
            console.log('delete job');
            let formData = {};
            formData['jobId'] = job.jobId;
            let promise = this.$store.dispatch('deleteJob', formData);
            promise.then((success) => {
                console.log(success);
            }, (error) => {
                console.log(error);
            })

        }
    }
    });