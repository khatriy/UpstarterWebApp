export default ({
   name:'updateJobFormComp',
    props:['job'],
    data(){
      return{
          jobDetails:{
              jobId:'',
              jobTitle:'',
              jobDescription:'',
              jobUrl:''
          }
      }
    },
    created(){
        this.jobDetails.jobId=this.job.jobId;
        this.jobDetails.jobTitle=this.job.Title;
        this.jobDetails.jobUrl=this.job.Url;
        this.jobDetails.jobDescription=this.job.Description;
    },
    computed:{
        formSubmit(){
            if(!this.errors.any())
            {
                var checkStatus=true;
                $.each(this.jobDetails,(key,value)=>{
                    if(value===''){
                        checkStatus=false;
                        return false;
                    }
                });
                return checkStatus;
            }

            else return false;
        }
    },
   template:`
   <div class="modal--main all">

        <div class="modal--background all">

        </div>

        <!--  Client MODAL   -->
        
                    <div class="panel panel-default modal--comp" style="padding: 0%;">
                       <div style="min-height: 625px; padding: 2%"><button @click="closeModal()" style="float: right;" class=" btn-default fa fa-lg fa-close"></button>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Job Form</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Fill the Detail
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form  @submit.prevent="validateBeforeSubmit()" role="form">
                                <div class="form-group">
                                    <label>Job Title</label>
                                    <input v-model="jobDetails.jobTitle" class="form-control"  name="jobTitle" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('jobTitle')">{{errors.first('jobTitle')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Url</label>
                                    <input v-model="jobDetails.jobUrl" class="form-control"  name="jobUrl" v-validate="'required|url'" type="text" >
                                    <p class="text-danger" v-if="errors.has('jobUrl')">{{errors.first('jobUrl')}}</p>
                                </div>
                                <div class="form-group">
                                            <label>Example</label>
                                            <p class="form-control-static">http://example.xyz</p>
                                        </div>
                                <div class="form-group">
                                    <label>job Description</label>
                                    <textarea v-model="jobDetails.jobDescription" class="form-control"  name="jobDescription" v-validate="'required'" type="text" ></textarea>
                                    <p class="text-danger" v-if="errors.has('jobDescription')">{{errors.first('jobDescription')}}</p>
                                </div>
             
                                <button v-if="formSubmit" type="submit" class="btn btn-default">Submit Button</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                    </div>
                    <!-- /.panel -->
                </div>
        

    
   `,
   methods: {
       validateBeforeSubmit(){
           if(this.formSubmit)
           {
               let formData={};
               formData['jobId']=this.jobDetails.jobId;
               formData['title']=this.jobDetails.jobTitle;
               formData['url']=this.jobDetails.jobUrl;
               formData['description']=this.jobDetails.jobDescription;
               let promise=this.$store.dispatch('updateJobDetails',formData);
               promise.then((success)=>{
                  this.closeModal();
                  console.log(success);
               },(error)=>{
                   console.log(error)
               })
           }
           else console.log('please fill the form correctly');

       },
       closeModal(){
           var data={};
           data['View']=false;
           data['Job']={};
           this.$store.commit('mutateCompanyUpdateForm',data);
       },


   }
});