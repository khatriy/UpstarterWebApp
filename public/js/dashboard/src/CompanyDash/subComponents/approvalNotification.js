import moment from 'moment'
import NotificationDetailComp from './Notifications/NotificationDetailComp.js'
Vue.prototype.moment = moment
export default ({
    name:'approvalNotification',
    // props:['notificationMessage'],
    components:{NotificationDetailComp},
    data(){
      return {
          panelShow:true,
          eventDetailToShow:{}
      }
    },
    computed:{
      notificationMessages(){
          return this.$store.state.CompanyNotifications;
      }
    },
    template:`<div class="DashBoardContentNew" >
    <div class="row">
    <div style="margin-left:10px; padding-top: 10px" v-if="panelShow" class="col-lg-7">
        <div class="panel panel-default">
            <div class="panel-heading">
            <button @click="hidePanel($event)" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               Notifications
            </div>
            <!-- /.panel-heading -->
            <div v-for="notificationMessage in notificationMessages" class="panel-body" style="padding: 5px">
               <div class="alert alert-success alert-dismissable" style="margin-bottom: 0px">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <span>{{getNotificationMessage(notificationMessage)}}</span >
                                  <div style="color: black">
                                  <span v-if="eventNotification(notificationMessage)" style="padding-right: 60%; cursor: pointer"><a @click="viewNotificationDetail(notificationMessage)">View Details</a></span>
                                    {{timeDiffComputed(notificationMessage.created_at)}}</div> 
               </div>
                           
            </div>
            <!-- .panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <div class="col-lg-4" style="padding-top: 10px">
    <NotificationDetailComp :notificationDetails="eventDetailToShow"></NotificationDetailComp>
</div>
    </div>
    
</div>`,
    methods:{
        hidePanel(event){
            event.preventDefault();
            this.panelShow=!this.panelShow;
        },
        timeDiffComputed(createdTime){
            let now=moment().format('YYYY-MM-DD HH:mm:ss'); let then=createdTime;
            return moment.duration(moment(now,'YYYY-MM-DD HH:mm:ss').diff(moment(then,'YYYY-MM-DD HH:mm:ss'))).humanize()+' ago'
        },
        getNotificationMessage(notification){
            if(notification.type!=='App\\Notifications\\EventNotificationToUsers'){
                return notification.data.message;
            }
            else if(notification.type==='App\\Notifications\\EventNotificationToUsers'){
                return "New Event Added"
            }
        },
        eventNotification(event){
            if(event.type==='App\\Notifications\\EventNotificationToUsers'){
                return true
            }else return false
        },
        viewNotificationDetail(event){
            this.eventDetailToShow=event;
        }
    }
})