export default ({
    name:'sideMenuComp',
    data(){
      return {
          toggle:false,
          activeClass:'fa fa-arrow-down',
          notActiveClass:'fa arrow'
      }
    },
    computed:{
        numOfNotifications(){
            let notifications=this.$store.state.CompanyNotifications;
            return Object.keys(notifications).length;
        }
    },
    template:`<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav in" id="side-menu">
                        <li>
                            <router-link to="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</router-link>
                        </li>
                        <li>
                            <router-link to="/Notifications" ><i class="fa fa-globe fa-fw"></i>Notifications<span class="NotificationPanel">{{numOfNotifications}}</span></router-link>
                        </li>
                        <li>
                            <router-link to="/profile"><i class="fa fa-edit fa-fw"></i>Profile</router-link>
                        </li>
                        <li>
                            <a @click="toggleClass()" href="#"><i class="fa fa-edit fa-fw"></i>Jobs<span style="float: right" :class="[toggle ? activeClass: notActiveClass]"></span></a>
                            <ul class="nav nav-second-level collapse " v-bind:class="{in:toggle}" aria-expanded="true" style="">
                                <li>
                                    <router-link to="/jobs">Display jobs</router-link>
                                </li>
                                <li>
                                    <router-link to="/addJob">Add New job</router-link>
                                </li>
                               
                            </ul>
                        </li>
                    </ul>          
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->`,
    methods:{
        toggleClass(){
            this.toggle=!this.toggle;
        }
    }
});