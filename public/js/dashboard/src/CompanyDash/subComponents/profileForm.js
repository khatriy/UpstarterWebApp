
export default ({
   name:'profileForm',
    mounted(){
      let profileDataTmp=this.$store.state.companyProfileData;
      console.log(profileDataTmp);
      console.log(typeof(profileDataTmp));
      let copieddData=Object.assign({},profileDataTmp);
      delete copieddData.cmpId;
      delete copieddData.cmLogo;
      delete copieddData.userId;
      delete copieddData.created_at;
      delete copieddData.updated_at;
      delete copieddData.cmpStatus;
      this.profileData=copieddData;
      this.cmpLogo='';
      console.log(this.profileData);

    },
    data(){
       return{
      profileData:{
          cmpName:'',
          cmpRegNumber:'',
          cmpNatureOfBusiness:'',
          cmpVatNumber:'',
          cmpAddress:'',
          cmpBillingAddress:'',

          telephone:'',
          cmpEmail:'',
          cmpAccPayEmail:'',
          cmpAccPayTelephone:'',
          cmpRepName:'',


      },
           cmpLogo:'',

       }
    },
    computed:{
        formSubmit(){
           if(!this.errors.any())
               return true;
            else return false;
        },
        submitButtonShow(){
            var checkStatus=true;
           $.each(this.profileData,(key,value)=>{
               if(value==''){
                   checkStatus=false;
                   return false;
               }
           });
           if(this.cmpLogo=="")
               checkStatus=false;
           return checkStatus;

        }
    },
    template:`<div class="ProfileFormWeb">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Profile Form</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div >
                
                <div class="formBody">
                    <div class="row">
                        
                            <form  @submit.prevent="validateBeforeSubmit()" role="form">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input v-model="profileData.cmpName" class="form-control"  name="companyName" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('companyName')">{{errors.first('companyName')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Company Registration Number</label>
                                    <input v-model="profileData.cmpRegNumber" class="form-control"  name="Registration Number" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Registration Number')">{{errors.first('Registration Number')}}</p>

                                </div>
                                <div class="form-group">
                                    <label>Nature of Business</label>
                                    <input v-model="profileData.cmpNatureOfBusiness" class="form-control"  name="Nature of Business" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Nature of Business')">{{errors.first('Nature of Business')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Vat Number</label>
                                    <input v-model="profileData.cmpVatNumber" class="form-control"  name="Vat Number" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Vat Number')">{{errors.first('Vat Number')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input v-model="profileData.cmpAddress" class="form-control"  name="Address" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Address')">{{errors.first('Address')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Billing Address</label>
                                    <input v-model="profileData.cmpBillingAddress" class="form-control"  name="Billing Address" v-validate="''" type="text" >
                                    <p class="text-danger" v-if="errors.has('Billing Address')">{{errors.first('Billing Address')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Choose Logo</label>
                                    <input v-on:change="onFileChange($event)" class="form-control"  name="Choose Logo" v-validate="'required|ext:jpg,jpeg,svg,png'" type="file" >
                                     <p class="text-danger" v-if="errors.has('Choose Logo')">{{errors.first('Choose Logo')}}</p>
                                    </div>
                                <div class="form-group">
                                    <label>Company Telephone</label>
                                    <input v-model="profileData.telephone" class="form-control"  name="Company Telephone" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Company Telephone')">{{errors.first('Company Telephone')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Company Email</label>
                                    <input v-model="profileData.cmpEmail" class="form-control"  name="Company Email" v-validate="'required|email'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Company Email')">{{errors.first('Company Email')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Company Account Payable Telephone</label>
                                    <input v-model="profileData.cmpAccPayTelephone" class="form-control"  name="Company Account Payable Telephone" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Company Account Payable Telephone')">{{errors.first('Company Account Payable Telephone')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Company Account Payable Email</label>
                                    <input v-model="profileData.cmpAccPayEmail" class="form-control"  name="Company Account Payable Email" v-validate="'required|email'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Company Account Payable Email')">{{errors.first('Company Account Payable Email')}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Applicant's Name</label>
                                    <input v-model="profileData.cmpRepName" class="form-control"  name="Applicants Name" v-validate="'required'" type="text" >
                                    <p class="text-danger" v-if="errors.has('Applicants Name')">{{errors.first('Applicants Name')}}</p>
                                </div>
                             
                                <button v-if="submitButtonShow" type="submit" class="btn btn-default">Submit Button</button>
                            </form>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`,
    methods:{
        onFileChange(event){
                let files=event.target.files|| event.dataTransfer.files;
            if(!files.length){
                return
            }
            this.cmpLogo=files[0];
        },
        validateBeforeSubmit(){
            if(this.formSubmit)
            {
                let formData=new FormData();
                formData.append('data',JSON.stringify(this.profileData));
                formData.append('image',this.cmpLogo);
                let promise=this.$store.dispatch('insertFromData',formData);
                promise.then((success)=>{
                    this.$router.push('/')
            },(error)=>{
                    console.log(error)
            })
            }
            else console.log('please fill the form correctly');

        }
    }
});
