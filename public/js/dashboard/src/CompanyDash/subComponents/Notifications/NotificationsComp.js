import approvalNotification from './../approvalNotification.js';
export default ({
   name:'NotificationsComp',
    components:{approvalNotification},
    created(){

    },
    computed:{
        numOfNotifications(){
            let notificationObject=this.$store.state.CompanyNotifications;
            if(Object.keys(notificationObject).length>0)
                return true
            else return false;
        }
    },
   template:`<div>
<approvalNotification v-if="numOfNotifications"></approvalNotification>
</div>`
});