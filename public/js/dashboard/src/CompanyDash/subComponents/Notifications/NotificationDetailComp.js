export default ({
    name:'NotificationDetailComp',
    props:['notificationDetails'],
    computed:{
        hasEvent(){
            return Object.keys(this.notificationDetails).length;
        }
    },
    template:`<div class="panel panel-default">
            <div class="panel-heading">
               Notification Detail
            </div>
            <div class="panel-body" style="min-height:250px; padding: 0px">
                            
                                <table v-if="hasEvent" class="table table-striped table-bordered table-hover">
                               
                                    <tbody>
                                        <tr>
                                            
                                            <td><b>Event Name</b></td>
                                            <td>{{notificationDetails.data.Title}}</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><b>Event Date</b></td>
                                            <td>{{notificationDetails.data.Date}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Time</b></td>
                                            <td>{{notificationDetails.data.Time}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Location</b></td>
                                            <td>{{notificationDetails.data.location}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Description</b></td>
                                            <td>{{notificationDetails.data.Description}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                          
                            <!-- /.table-responsive -->
                   
                </div>
            </div>
            <!-- /.panel-heading -->
           
            <!-- .panel-body -->
        </div>`,
    methods:{
    }
})