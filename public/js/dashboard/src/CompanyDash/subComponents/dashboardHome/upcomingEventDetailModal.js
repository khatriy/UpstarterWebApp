export default ({
    name:'upComingEventDetailModal',
    props:['eventDetails'],
    data(){
        return {
            standChoice:'Platinum',
            bookButtonPage:true,
            confirmPage:false,
        }
    },
    computed:{
        checkForEvent(){
            let option=false;
            let eventId=this.eventDetails.eventId;
            let eventRegisteredFor=this.$store.state.EventsCompanyRegisteredFor;
            let i;
            for(i=0;i<Object.keys(eventRegisteredFor).length;i++){
                console.log(event);
              if(eventRegisteredFor[i].eventId===eventId){
                  option=true;
                  break;
              }
            }
            return option;
        },
        buttonText(){
            if(this.checkForEvent){
                return 'Already Registered for Event!!';
            }
            else {
                return 'Book A Place';
            }
        }
    },
    template:` <div class="modal--main all">

        <div class="modal--background all">

        </div>

        <!--  Client MODAL   -->
       
    <div class="panel panel-default modal--comp" style="padding-top: 0px; min-height: 250px; overflow: hidden">
      <button @click="closeModal()" style="float: right;padding:1%; margin-bottom: 2%" class=" btn-default fa fa-lg fa-close"></button>
      <div class="panel-heading">
            Event Detail
        </div>
        <transition name="fade" v-on:after-leave="afterLeave">
            <div v-if="bookButtonPage" class="panel-body" style="min-height:250px; padding: 0px">
         <table class="table table-striped table-bordered table-hover">
                               
                                    <tbody>
                                        <tr>
                                            
                                            <td><b>Event Name</b></td>
                                            <td>{{eventDetails.Title}}</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><b>Event Date</b></td>
                                            <td>{{eventDetails.Date}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Time</b></td>
                                            <td>{{eventDetails.Time}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Location</b></td>
                                            <td>{{eventDetails.location}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Description</b></td>
                                            <td>{{eventDetails.Description}}</td>
                                        </tr>
                                    </tbody>
                                </table>
       <div style="text-align: center">
       <a class="btnEventReg" v-bind:class="{buttonDisable:checkForEvent}" @click="bookForEvent()">{{buttonText}}</a>  
       </div>
      </div>
        </transition>
      <transition name="fade">
         <div v-if="confirmPage" class="panel-body" style="min-height:250px; padding: 5%">
       <div class="containerCustom">
		<form @submit.prevent="validateBeforeSubmit(eventDetails.eventId)" class="formCustom cf">
			<section class="plan cf">
				<h2>Stand Choice:</h2>
				<input v-model="standChoice" type="radio" name="radio1" id="Platinum" value="Platinum"><label class="free-label four colCustom" for="Platinum">Platinum</label>
				<input v-model="standChoice" type="radio" name="radio1" id="Gold" value="Gold" checked><label class="basic-label four colCustom" for="Gold">Gold</label>
				<input v-model="standChoice" type="radio" name="radio1" id="Silver" value="Silver"><label class="premium-label four colCustom" for="Silver">Silver</label>
				<input v-model="standChoice" type="radio" name="radio1" id="Bronze" value="Bronze"><label class="premium-label four colCustom" for="Bronze">Bronze</label>
				<input v-model="standChoice" type="radio" name="radio1" id="Startup" value="Startup"><label class="premium-label four colCustom" for="Startup">Startup</label>

			</section>
			<div style="text-align: center"><input class="submitCustom" type="submit" value="Confirm"></div>	
		</form>
	</div>
       </div>
      </transition>
     
    </div>
                    <!-- /.panel -->
                </div>`,
    methods:{
        closeModal(){
            this.$store.commit('mutateUpComingEventDetailModalShow',false);
        },
        bookForEvent(){
            this.bookButtonPage=false;
        },
        afterLeave(){
            this.confirmPage=true;
        },
        validateBeforeSubmit(eventId){
            console.log(eventId);
            this.$store.commit('mutateUpComingEventDetailModalShow',false);
            let formData={};
            formData['eventId']=eventId;
            formData['standChoice']=this.standChoice;
            let promiseVar=this.$store.dispatch('createAPlaceForCompany',formData);
            promiseVar.then((success)=>{
                console.log(success);
            },(error)=>{
                console.log(error)
            })
        }
    }
});