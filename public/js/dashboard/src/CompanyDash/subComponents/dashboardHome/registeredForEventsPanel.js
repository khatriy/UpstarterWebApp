export default ({
    name:'registeredForEventsPanel',
    components:{},
    data(){
        return {

        }
    },
    created(){
        let promiseVar=this.$store.dispatch('getAllEventsCompaniesRegisteredFor');
        promiseVar.then((success)=>{
            console.log('getAllUpcomingEvents promise success with '+success);
        },(error)=>{
            console.log('getAllUpcomingEvents promise error with '+error);
        })
    },
    computed:{
        numOfRegisteredEvents(){
            return Object.keys(this.$store.state.EventsCompanyRegisteredFor).length;
        },
        RegisteredEventsByCompany(){
            return this.$store.state.EventsCompanyRegisteredFor;
        }


    },
    template:` 
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Events Attending..<span class="fa fa-thumb-tack" style="float: right;"></span>
                        </div>
                        <!-- /.panel-heading -->
                        <div v-if="numOfRegisteredEvents===0" class="panel-body DashPanel">
                            <div class="alert alert-danger">
                                You haven't register for any event yet!! Please checkout <b>Upcoming Events Panel</b> for upcoming events. 
                            </div>
                        </div>
                        <div v-if="numOfRegisteredEvents!==0" class="panel-body DashPanel" style="padding: 5px">
                                <div v-for="(eventRegistered,index) in RegisteredEventsByCompany" class="alert alert-success">
                                   <span><b>{{eventRegistered.Title}}</b></span >
                                  <div style="color: black">
                                  {{eventRegistered.Date}}
                                  <span style="padding-left: 40%; cursor: pointer"><a @click="viewEventDetail(eventRegistered)">View Details</a></span>
                                  </div>   
                           
                          </div>  
                        <!-- .panel-body -->
                    </div>
                </div>
                <!--<notificationDetailModal v-if="notificationDetailModalShow" :notificationDetails="notificationDetailToShow"></notificationDetailModal>-->
               </div>`,
    methods:{
        viewEventDetail(event){

        }
    }
});