import updateJobFormComp from './../updateJobFromComp';
export default ({
    name:'postedJobsPanel',
    components:{updateJobFormComp},
    created(){
        let promiseVar=this.$store.dispatch('getAvailableJobs');
        promiseVar.then((success)=>{
            console.log('getAvailableJobs promise success with '+success);
        },(error)=>{
            console.log('getAvailableJobs promise error with '+error);
        })
    },
    computed:{
        PostedJobs(){
            return this.$store.state.CompanyAddedJobs;
        },
        numOfJobsPosted(){
            return Object.keys(this.$store.state.CompanyAddedJobs).length;
        },
        updateFormShow(){
            return this.$store.state.CompanyUpdateProfileFormView;
        },
        job(){
            return this.$store.state.CompanyUpdateProfileFormViewJob;
        }
    },
    template:` <div class="col-lg-4">
 <updateJobFormComp v-if="updateFormShow" :job="job"></updateJobFormComp>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Jobs Posted<span class="fa fa-thumb-tack" style="float: right;"></span>
                        </div>
                        <div v-if="numOfJobsPosted===0" class="panel-body DashPanel">
                                 <div class="alert alert-info">No Jobs Posted Yet!! Click here to <router-link to="/addJob" class="alert-link">Add New</router-link> Job.</div>   
                        </div>
                        <!-- /.panel-heading -->
                        <div v-if="numOfJobsPosted!==0" class="panel-body DashPanel">
                            <div v-for="(job,index) in PostedJobs" class="alert alert-success">
                                <b>{{job.Title}}</b>
                                <div><a style="cursor:pointer;" @click="updateJob(job)">View Details</a></div>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    
                </div>`
    ,
    methods:{
        updateJob(job) {
            var data = {};
            data['View'] = true;
            data['Job'] = job;
            this.$store.commit('mutateCompanyUpdateForm', data);

            console.log('updateJob');
        },
    }
});