import moment from 'moment'
import notificationDetailModal from './notificationDetailShowModal';
Vue.prototype.moment = moment
export default ({
   name:'notificationPanel',
    components:{notificationDetailModal},
    data(){
      return {
          notificationDetailShow:false,
          notificationDetailToShow:{}
      }
    },
    created(){

    },
    computed:{
        notificationMessages(){
         return this.$store.state.CompanyNotifications;
      },
        numOfNotifications(){
         return Object.keys(this.$store.state.CompanyNotifications).length;
        },
        notificationDetailModalShow(){
            return this.$store.state.notificationDetailModalShow;
        }
    },
   template:` <div style="float: left;width: 100%;">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Notifications<span class="fa fa-thumb-tack" style="float: right;"></span>
                        </div>
                        <!-- /.panel-heading -->
                        <div v-if="numOfNotifications===0" class="panel-body DashPanel">
                            <div class="alert alert-danger">
                                No New Notifications!!
                            </div>
                        </div>
                        <div v-if="numOfNotifications!==0" class="panel-body DashPanel" style="padding: 5px">
                                <div v-for="(notificationMessage,index) in notificationMessages" class="alert alert-success">
                                    <span><b>{{getNotificationMessage(notificationMessage)}}</b></span >
                                  <div style="color: black">
                                  <span v-if="eventNotification(notificationMessage)" style="padding-right: 40%; cursor: pointer"><a @click="viewNotificationDetail(notificationMessage)">View Details</a></span>
                                    {{timeDiffComputed(notificationMessage.created_at)}}</div> 
                                    </div>   
                           
                          </div>  
                        <!-- .panel-body -->
                    </div>
                </div>
                <notificationDetailModal v-if="notificationDetailModalShow" :notificationDetails="notificationDetailToShow"></notificationDetailModal>
               </div>`,
    methods:{
        timeDiffComputed(createdTime){
            let now=moment().format('YYYY-MM-DD HH:mm:ss'); let then=createdTime;
            return moment.duration(moment(now,'YYYY-MM-DD HH:mm:ss').diff(moment(then,'YYYY-MM-DD HH:mm:ss'))).humanize()+' ago'
        },
        getNotificationMessage(notification){
            if(notification.type!=='App\\Notifications\\EventNotificationToUsers'){
                return notification.data.message;
            }
            else if(notification.type==='App\\Notifications\\EventNotificationToUsers'){
                return "New Event Added"
            }
        },
        eventNotification(event){
            if(event.type==='App\\Notifications\\EventNotificationToUsers'){
                return true
            }else return false
        },
        viewNotificationDetail(notification){
            this.notificationDetailToShow=notification;
            this.$store.commit('mutateNotificationDetailModalShow',true);
        }
    }
});