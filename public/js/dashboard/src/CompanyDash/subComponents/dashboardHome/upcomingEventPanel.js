import upComingEventDetailModal from './upcomingEventDetailModal';
export default ({
    name:'upcomingEventsPanel',
    components:{upComingEventDetailModal},
    data(){
      return {
          eventDetailToShow:{}
      }
    },
    created(){
      let promiseVar=this.$store.dispatch('getAllUpcomingEvents');
      promiseVar.then((success)=>{
          console.log('getAllUpcomingEvents promise success with '+success);
      },(error)=>{
          console.log('getAllUpcomingEvents promise error with '+error);
      })
    },
    computed:{
      upcomingEvents(){
          return this.$store.state.UpcomingEventForCompanyDash;
      }  ,
      numOfUpComingEvents(){
          return Object.keys(this.$store.state.UpcomingEventForCompanyDash).length;
      },
        upComingEventDetailModalShow(){
          return this.$store.state.upComingEventDetailModalShow;
        }
    },
    template:` <div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            Upcoming Events<span class="fa fa-thumb-tack" style="float: right;"></span>
                        </div>
                        <!-- /.panel-heading -->
                        <div  class="panel-body DashPanel" v-if="numOfUpComingEvents===0">
                             <div class="alert alert-info">
                                No Upcoming Events!!
                            </div>
                        </div>
                        <div v-if="numOfUpComingEvents!==0"  class="panel-body DashPanel">
                            <div v-for="(upcomingEvent,index) in upcomingEvents" class="alert alert-success">
                                    <span><b>{{upcomingEvent.Title}}</b></span >
                                  <div style="color: black">
                                  {{upcomingEvent.Date}}
                                  <span style="padding-left: 40%; cursor: pointer"><a @click="viewEventDetail(upcomingEvent)">View Details</a></span>
                                  </div> 
                             </div> 
                           
                        </div>
                      
                        <!-- .panel-body -->
                    </div>
                    </div>
                     <upComingEventDetailModal v-if="upComingEventDetailModalShow" :eventDetails="eventDetailToShow"></upComingEventDetailModal>
                </div>`,
    methods:{
        viewEventDetail(event){
            this.eventDetailToShow=event;
            this.$store.commit('mutateUpComingEventDetailModalShow',true);
        }
    }
});