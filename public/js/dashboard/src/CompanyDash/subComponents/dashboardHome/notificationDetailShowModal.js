export default ({
    name:'notificationDetailModal',
    props:['notificationDetails'],
    template:` <div class="modal--main all">

        <div class="modal--background all">

        </div>

        <!--  Client MODAL   -->
       
    <div class="panel panel-default modal--comp" style="padding-top: 0px">
      <button @click="closeModal()" style="float: right;padding:1%; margin-bottom: 2%" class=" btn-default fa fa-lg fa-close"></button>
      <div class="panel-heading">
            Notification Detail
        </div>
      <div class="panel-body" style="min-height:250px; padding: 0px">
                            
                                <table class="table table-striped table-bordered table-hover">
                               
                                    <tbody>
                                        <tr>
                                            
                                            <td><b>Event Name</b></td>
                                            <td>{{notificationDetails.data.Title}}</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><b>Event Date</b></td>
                                            <td>{{notificationDetails.data.Date}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Time</b></td>
                                            <td>{{notificationDetails.data.Time}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Location</b></td>
                                            <td>{{notificationDetails.data.location}}</td>
                                        </tr>
                                        <tr>
                                          
                                            <td><b>Event Description</b></td>
                                            <td>{{notificationDetails.data.Description}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                          
                            <!-- /.table-responsive -->
                   
             
   
</div>
    </div>
                    <!-- /.panel -->
                </div>`,
    methods:{
        closeModal(){
            this.$store.commit('mutateNotificationDetailModalShow',false);
        }
    }
});