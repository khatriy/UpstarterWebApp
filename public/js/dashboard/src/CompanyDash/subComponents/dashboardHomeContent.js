import upcomingEventsPanel from './dashboardHome/upcomingEventPanel';
import notificationPanel from './dashboardHome/notificationPanel';
import postedJobsPanel from './dashboardHome/PostedJobsPanel';
import registeredForEventsPanel from './dashboardHome/registeredForEventsPanel';
export default ({
    name:'dashboardHomeContent',
    components:{upcomingEventsPanel,notificationPanel,postedJobsPanel,registeredForEventsPanel},
    created(){
            var Promise=this.$store.dispatch('getCompanyNotifications');
            Promise.then((success)=>{
                if(Object.keys(success).length>0)
                console.log('notification promise succeed with notifications' );
                else
                    console.log('notification promise succeed withOut notifications' );

            },(error)=>{
                console.log('notification promise failed '+error);
            })
    },
    computed:{
        numOfNotifications(){
          let notificationObject=this.$store.state.CompanyNotifications;
          if(Object.keys(notificationObject).length>0)
              return true
            else return false;
        },
      verifiedByAdmin(){
          let cmpStatus=this.$store.state.companyProfileData.cmpStatus;
          if(cmpStatus!=='')
          return cmpStatus;
          else return 0
      }
    },
    data(){
      return{
          notificationMessage:'Application is under review, You will soon get a notification once it would be approved by the admin.',
      }
    },
    template:`<div style="padding-top: 2%" class="DashBoardContentNew">
               <upcomingEventsPanel></upcomingEventsPanel>
               <postedJobsPanel></postedJobsPanel>
               <registeredForEventsPanel></registeredForEventsPanel>
                <notificationPanel></notificationPanel>
              </div>`
});