import profileForm from './../subComponents/profileForm.js';
import dashboardHomeContent from './../subComponents/dashboardHomeContent.js';
import jobsComp from './../subComponents/jobsComp.js';
import createJobForm from './../subComponents/createJobForm.js';
import NotificationsComp from './../subComponents/Notifications/NotificationsComp'
export default new VueRouter({
    routes:[
        {path:'/',name:'home',component:dashboardHomeContent},
        {path:'/profile',component:profileForm},
        {path:'/jobs',component:jobsComp},
        {path:'/addJob',component:createJobForm},
        {path:'/Notifications',component:NotificationsComp}
        // {path:'/SessionDetail',name:'sessionDetail',component:sessionDefault},
        // { path: '/feed',name:'home',component: feedShowComponent },
        // {
        //     path:'/test',name:'test',component:dummyComponent
        // }
    ]
});