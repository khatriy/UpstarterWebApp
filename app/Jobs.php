<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table='companyjobs';
    protected $primaryKey='jobId';
    protected $guarded = [];
}
