<?php

namespace App\Jobs;

use App\Mail\EventNotifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class SendEventNotificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user;
    public function __construct($user)
    {
        $this->user=$user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      try{

          $user=$this->user;
          $data['users']=$user;
          \Log::info($data);
          Mail::send('mails.testMail',$data,function ($message) use ($user){
              $message->from('yashkhatri.001@gmail.com','Yash Khatri');
              $message->to($user->email,$user->name);
      });
      } catch (Exception $exception){
          \Log::info($exception->getMessage());
      }
    }
}
