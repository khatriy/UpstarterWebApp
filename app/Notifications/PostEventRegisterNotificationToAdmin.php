<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;

class PostEventRegisterNotificationToAdmin extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $companyEventDetail;
    private $companyDetails;
    public function __construct($companyDetails,$companyEventDetail)
    {
        $this->companyEventDetail=$companyEventDetail;
        $this->companyDetails=$companyDetails;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $arrayToBeReturned=[];
        $arrayToBeReturned['companyDetails']=$this->companyDetails;
        $arrayToBeReturned['companyEventDetails']=$this->companyEventDetail;
        Log::info($arrayToBeReturned);
        return $arrayToBeReturned;
    }
}
