<?php

namespace App\Http\Controllers;

use App\Companies;
use App\CompanyAtEvents;
use App\Events;
use App\Jobs;
use App\Notifications\PostEventRegisterNotificationToAdmin;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class CompanyController extends Controller
{
    public function getCompanies(){

        return view('ListOfCompanies');
    }
    public function getNotifications(){
        $user=User::find(\Auth::user()->id);
        $notifications=array();
        forEach($user->notifications as $notification){
                array_push($notifications,$notification);
        }
       $user->unreadNotifications->markAsRead();
        return json_encode($notifications);


    }
    public function addNewJob(Request $request){
        $company=Auth::user();
        $companyId=$company->id;
        $jobDetails=[];
        $jobDetails['companyId']=$companyId;
        $error=0;
       if($request->has('title')){
           $title=$request->input('title');
           $jobDetails['Title']=$title;
       }else $error=1;
        if($request->has('url')){
            $url=$request->input('url');
            $jobDetails['Url']=$url;
        }else $error=1;
        if($request->has('description')){
            $description=$request->input('description');
            $jobDetails['Description']=$description;
        }else $error=1;
        if($error!=1){
            $jobs=new Jobs();
            $jobs->Title=$title;
            $jobs->Description=$description;
            $jobs->Url=$url;
            $jobs->companyId=$companyId;
            if($jobs->save()){
                flash('Job Successfully Added')->success();
                return json_encode(true);
            }
            else {
                flash('Something Went Wrong!')->error();
                return json_encode(false);
            }
        }
        else return json_encode(false);

    }
    public function getAddedJobs(Request $request){
        $jobs=Jobs::all()->where('companyId',Auth::user()->id);
        if(sizeof($jobs)!=0){
            return json_encode($jobs);
        }
        else return json_encode(false);
    }
    public function updateJob(Request $request){
        $jobId=$request->input('jobId');
        $jobObject=Jobs::find($jobId);
        $jobObject->Title=$request->input('title');
        $jobObject->Url=$request->input('url');
        $jobObject->Description=$request->input('description');
        if($jobObject->save()){
            $jobs=Jobs::all()->where('companyId',Auth::user()->id);
            if(sizeof($jobs)!=0){
                return json_encode($jobs);
            }
            else return json_encode(false);
        }
        else return json_encode(false);
    }
    public function deleteJob(Request $request){
        $jobId=$request->input('jobId');
        Jobs::destroy($jobId);
        $jobs=Jobs::all()->where('companyId',Auth::user()->id);
        return json_encode($jobs);

    }
    public function getUpcomingEvents(){
        $Now=Carbon::now()->format('Y-m-d');
        $upcomingEvents=Events::where('Date','>=',$Now)->get();
        return json_encode($upcomingEvents->toArray());
    }
    public function addNewAttendee(Request $request){
        $user=Auth::user();
        $userId=$user->id;
        $company=Companies::where('userId',$userId)->get()->first();
        $companyId=$company->cmpId;
        if($companyId){
            $eventId=$request->input('eventId');
            $standChoice=$request->input('standChoice');
            $company_event=new CompanyAtEvents();
            $company_event->cmpId=$companyId;
            $company_event->eventId=$eventId;
            $company_event->standChoice=$standChoice;
            try{
                if($company_event->save()) {
                    $admin=User::where('userType',2)->get()->first();
                    $company_event_instance=CompanyAtEvents::where(['cmpId'=>$companyId,'eventId'=>$eventId])->get()->first();
                    $event=Events::where('eventId',$eventId)->first()->getAttributes();
                    $company=$company->getAttributes();
                    $company_event_instance=$company_event_instance->getAttributes();
                    $company_event_instance['eventName']=$event['Title'];
                    $admin->notify(new PostEventRegisterNotificationToAdmin($company,$company_event_instance));
                    return $this->getEventsCompaniesRegisteredFor();
                }
            }catch (Exception $exception){
                return json_encode($exception->getMessage());
            }

        }
    }
    public function getEventsCompaniesRegisteredFor(){
        $user=Auth::user();
        $userId=$user->id;
        $company=Companies::where('userId',$userId)->get()->first();
        $companyId=$company->cmpId;
        $eventsCompanyRegisteredFor=CompanyAtEvents::where('cmpId',$companyId)->get();
        $eventsCompanyRegisteredForArray=$eventsCompanyRegisteredFor->toArray();
        $eventArrayToBeReturned=array();
        foreach ($eventsCompanyRegisteredForArray as $eventCompanyInstance){
            $eventId=$eventCompanyInstance['eventId'];
            $event=Events::find($eventId);
            array_push($eventArrayToBeReturned,$event);
        }
        return json_encode($eventArrayToBeReturned);

    }
}
