<?php

namespace App\Http\Controllers;

use App\Companies;
use App\Notifications\newCompanyRegistration;
use App\Notifications\ToTheCompanies;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class DashboardController extends Controller
{
    public function getDashboard(){
        return view('dashboard');
    }
    public function getUser(){
        if(Auth::check()) {
            return json_encode(Auth::user());
        }else{
            return json_encode(false);
        }
    }
    public function getCompanyProfile(){
        $user_id=Auth::user()->id;
        $companyData=Companies::where('userId',$user_id)->first();
        if(!empty($companyData)){
            return json_encode($companyData);
        }

        else {return json_encode(array());}

    }
    public function testNotify(){

    }
    public function submitProfile(Request $request){
        $partialData=$request->all();
        $dataObject=$partialData['data'];
        $dataObject=json_decode($dataObject);
        $profileData=[];
        foreach ($dataObject as $key=>$value){
           $profileData[$key]=$value;
        }
        $userId=Auth::user()->id;
        if($request->hasFile('image')){
            $imagePath=$request->file('image')->store('logos');
        }
        $cmpStatus=0;
        $profileData['userId']=$userId;
        $profileData['cmpLogo']=$imagePath;
        $profileData['cmpStatus']=$cmpStatus;
        $updateOrCreate=Companies::where('userId',Auth::user()->id)->first();
        if(empty($updateOrCreate)){
            $first=Companies::updateOrCreate(['userId'=>$userId],$profileData);
            $user=User::where('userType',2)->first();
            $company=Auth::user();
            $messageToCompany="Application is under review, You will soon get a notification once it would be approved by the admin.";
            try{
                $user->notify(new newCompanyRegistration($first->toArray()));
                $company->notify(new ToTheCompanies($messageToCompany));

            }catch (Exception $exception){
                return dd($exception->getMessage());
            }
            return json_encode($first);
        }
        else{
            $first=Companies::updateOrCreate(['userId'=>$userId],$profileData);
            return json_encode($first);

        }


    }
}
