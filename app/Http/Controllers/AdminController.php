<?php

namespace App\Http\Controllers;

use App\Companies;
use App\Events;
use App\Jobs\SendEventNotificationEmail;
use App\Mail\EventNotifyEmail;
use App\Notifications\confirmatioFromAdmin;
use App\Notifications\EventNotificationToUsers;
use App\User;
use Egulias\EmailValidator\Exception\ExpectedQPair;
use Illuminate\Http\Request;
use Mockery\Exception;
use PhpParser\Node\Expr\Cast\Array_;
use function PHPSTORM_META\elementType;

class AdminController extends Controller
{
    public function getNotifications(){
        $user=User::find(\Auth::user()->id);
        $notifications=array();
       forEach($user->unreadNotifications as $notification){
//           if($notification->type=='App\Notifications\newCompanyRegistration'){
               array_push($notifications,$notification);
//           }
       }
       return json_encode($notifications);
    }
    public function authorizeCompany(Request $request){
        if($request->has('cmpId')){
            $company=Companies::find($request->input('cmpId'));
            if($company){
                $company->cmpStatus=1;
                $company->save();
            }
            if($request->has('userId')){
                $company=User::find($request->input('userId'));
                $company->notify(new confirmatioFromAdmin("Request approved"));
                if($request->has('notId')){
                    $adminUser=\Auth::user();
                    $notification=$adminUser->notifications()->where('id',$request->input('notId'))->first();
                    if($notification){
                        $notification->delete();
                        return json_encode(true);
                    }
                }
            }
            try{

            }catch (Exception $exception){
                return json_encode($exception->getMessage());
            }
        }else return json_encode(false);
    }
    public function getRegCompanies(Request $request){
        $regCompanies=Companies::all();
        return json_encode($regCompanies->toArray());
    }
    public function cmpStatusUpdate(Request $request){

        try{
            $cmpId=$request->input('cmpId');
            $changedStatus=$request->input('changedStatus');
            $company=Companies::find($cmpId);
            $company->cmpStatus=$changedStatus;
            if($company->save()){
                return json_encode(true);
            }
            else return json_encode(false);
        }catch (Exception $exception){
            return json_encode(false);
        }


    }
    public function getApprovedCompanies(Request $request){
        $companies=Companies::where('cmpStatus',1)->get();
        return json_encode($companies->toArray());
    }
    public function getDeclinedCompanies(Request $request){
        $companies=Companies::where('cmpStatus',0)->get();
//        var_dump(gettype($companies->toArray()));
        return json_encode($companies->toArray());
    }
    public function addEvent(Request $request){
        $Event=new Events();
        $Event->Title=$request->input('eventTitle');
        $Event->Description=$request->input('eventDescription');
        $Event->Date=$request->input('eventDate');
        $Event->Time=$request->input('eventTime');
        $Event->location=$request->input('eventLocation');
        if($Event->save()){
            $users=User::all();
            foreach ($users as $user){
                $user->notify(new EventNotificationToUsers($Event->toArray()));
                $this->dispatch(new SendEventNotificationEmail($user));
            }
            return json_encode(true);
        }
    }
    public function getEvents(){
        $events=Events::all();
        return json_encode($events);
    }
    public function updateEvent(Request $request){
        $eventId=$request['eventId'];
        $eventTuple=Events::find($eventId);
        $eventTuple->Title=$request->input('eventTitle');
        $eventTuple->Date=$request->input('eventDate');
        $eventTuple->Time=$request->input('eventTime');
        $eventTuple->location=$request->input('eventLocation');
        $eventTuple->Description=$request->input('eventDescription');
        if($eventTuple->save()){
            return json_encode(Events::all());
        }
        else{
            return json_encode(false);
        }


    }
    public function deleteEvent(Request $request){
        if($request->has('eventId'))
            $eventId=$request->input('eventId');
        else
            return json_encode($request->all());
        try{
            Events::destroy($eventId);
            return json_encode(Events::all());

        }catch (Exception $exception){
            return json_encode($exception->getCode());
        }
    }
    public function getRegisteredUsers(Request $request){
        $registeredUsers=User::where('UserType',0)->get();
        return json_encode($registeredUsers->toArray());
    }
    public function getRegisteredCompanies(){
        $registeredComapnies=User::where('UserType',1)->get();
        return json_encode($registeredComapnies);
    }
}

