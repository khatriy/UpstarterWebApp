<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyAtEvents extends Model
{
    protected $table='company_events';
    protected $primaryKey='id';
    protected $guarded=[];
}
