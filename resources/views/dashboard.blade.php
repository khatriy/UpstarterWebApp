@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/dashboard/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/dashboard/metisMenu.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/dashboard/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('css/dashboard/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/dashboard/bootstrap-fs-modal.css')}}" rel="stylesheet">
    <link href="{{asset('css/vueTransition.css')}}" rel="stylesheet">
    @endsection

@section('content')
    <div id="dashBoardMain"></div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.3/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuex/2.3.1/vuex.js"></script>
    <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/metisMenu.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/sb-admin-2.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dashboard/dist/dashMainBuild.js')}}"></script>


    <script>Vue.use(VeeValidate);</script>
    @endsection