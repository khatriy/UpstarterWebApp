@extends('layouts.homePage')

@section('content')
    @for($i=0;$i<sizeof($items)%3;$i++)
        <div class="card-group">
    @for($j=0;$j<3;$j++)

            <div  class="card" style=" border-radius: 5px; padding: 1%; margin: 1% ">
                <img class="card-img-top" src="{{asset('images/download.svg')}}" alt="Card image cap" width="235px" height="180">
                <div class="card-block">
                    <h4 class="card-title">Company Name</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <router-link to="company/kere-soft" class="btn btn-primary">View More</router-link>
                </div>
            </div>

        @endfor
        </div>
    @endfor

    @endsection