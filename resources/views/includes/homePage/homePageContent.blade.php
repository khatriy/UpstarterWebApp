<div id="preloader">
        <div class="pre_container">
            <div class="loader_3">
            </div>
        </div>
    </div>
    <!-- Fixed navbar -->
    @include('includes.homePage.homePageHeader')
    <header>
        <div class="fill_mask">
            <video class="vid" autoplay muted loop poster="" src="{{asset('/videos/upstarter.webm#t=19')}}" type="video/webm">
                <source src="{{asset('videos/upstarter.webm#t=19')}}" type="video/webm">
            </video>
            <div class="description">
                <img src="{{asset('images/upstarter-logo-white.png')}}" alt="img09" style="width: 50%;height: auto;padding: 15px;">
            </div>
            <br>
        </div>
        <div class="buttons ">
            <a class="wow fadeInLeft btn eventime_button" href="#contact_menu" role="button">
                Display at UpStarter </a>
            <a class="wow fadeInRight btn eventime_button" href="https://www.eventbrite.ie/e/upstarter-dublin-october-edition-tickets-36859208886" role="button">
                Register </a>
        </div>
        </div>
        </div>
    </header>
    <div class="wow fadeIn tabs_about">
        <div class="mask" style="background-color:white">
            <div class="tab-content">
                <div id="sectionA" class="tab-pane fade in active">
                    <ul class="stats">
                        <li class="col-md-4">
                            <p>
              <span class="count">
                3,500 </span>
                                <span class="count_plus">
                  + </span>
                            </p>
                            <p class="count_dscrp">
                                Attendees
                            </p>
                        </li>
                        <li class="col-md-4">
                            <p>
                  <span class="count">
                    70 </span>
                                <span class="count_plus">
                      + </span>
                            </p>
                            <p class="count_dscrp">
                                Companies
                            </p>
                        </li>
                        <li class="col-md-4">
                            <p>
                      <span class="count">
                        945,000 </span>
                                <span class="count_plus">
                          + </span>
                            </p>
                            <p class="count_dscrp">
                                Social Reach
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="gap"></div>

    <div class="clear">
    </div>


    <div id="about_menu" class="wow fadeIn">
        <div class="contact ">
            <div class="mask">
                <div class="gap"></div>
                <div class="first_line">
                </div>
                <div class="first_title">
                    <p>
                        About
                    </p>
                </div>
                <div class="first_description">
                    <p>
                        UpStarter provides candidates with a chance to meet some of Ireland's most innovative companies who are all hiring. Employers on the night will be looking for developers, designers, marketers and students looking for internships.

                        If, on the other hand, you are on the lookout for a co-founder for your own startup, UpStarter will also provide you with a platform to meet other like minded people in a casual setting.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="testimonial-container">
        <div class="dk-container">
            <div class="cd-testimonials-wrapper cd-container">
                <div class="testimonials_line">
                </div>
                <h2 class="first_title testimonials_title">Testimonials</h2>
                <ul class="cd-testimonials">
                    <li>
                        <div class="testimonial-content">
                            <p>UpStarter provided us with a great forum for talking to experienced applicants and gaining an understanding into what they're looking for and what's important to them in a new role. </p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>Orla O’Connor, People Operations Lead, <strong>Teamwork.com</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>Relaxed, informal event which allowed us to interact with a range of candidates. It gave us the opportunity to inform potential candidates about our cyber, data analytics and technology positions along with our professional services opportunities.</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>Pat Moran, Head of Cyber & <strong>IT Forensics, PWC</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>Upstarter is a great way to network, we met some amazing people and will hopefully make a few hires based on the event!</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>Caroline Carey, Recruitment Coordinator, <strong>Zendesk</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>UpStarter is a great event for anyone looking to meet candidates and get their brand out there.</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>David Burke, VP of Engineering, <strong>Datahug</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>UpStarter was a great way for us to meet candidates and talk to them about the work we're doing. We’re already looking forward to the next event!</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>Tori Hughes, UI Designer, <strong>Showtime Analytics</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>UpStarter really is Ireland's premier tech hiring event.</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>Aonghus Shortt, CEO, <strong>Foodmarble</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>UpStarter was a well organised event with lots of enthusiastic tech professionals. We’re already seeing the benefits of being a part of UpStarter already.</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>Peter Mulvihill, Technology Advisor, <strong>Mason Alexander</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>A great event! It was very well organised and easy to attend. We have a list of good candidates to follow up with to fill our open roles, and mostly importantly we had lots of fun!</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>Susan Nolan, Operations Manager, <strong>LearnUpon</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="testimonial-content">
                            <p>FoodCloud benefited thoroughly from attending UpStarter. We had so many great conversations with lots of potential hires.</p>
                            <div class="cd-author">
                                <ul class="cd-author-info">
                                    <li>James Clifford, Software Engineer, <strong>FoodCloud</strong><br></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
            <!-- cd-testimonials -->
        </div>
    </div>

    <div class="gap"></div>

    <div id="about_menu" class="wow fadeIn">
        <div class="contact ">
            <div class="mask">

                <div id="speakers" class="photo_carousel carousel slide wow fadeInUp" style="padding-bottom: 0px">
                    <div class="col-xs-1 col-sm-1">

                    </div>
                    <div class="col-xs-12 col-sm-5 col-m-5 sponsored">
                        <h2 style="color:white">Sponsored by</h2>
                        <a class="" href="https://www.accenture.com/ie-en" target="_blank"><img src="images/accenture_logo.png" width="320" height="100" alt=" " style="margin-top: 5%"></a>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-m-5 sponsored">
                        <h2 style="color:white">In association with</h2>
                        <a class="" href="http://www.techireland.org" target="_blank"><img src="images/ti_logo.png" width="290" height="60" alt=" " style="margin-top: 5%"></a>
                        <a class="" href="http://www.irishtimes.com" target="_blank"><img src="images/it_logo.png" width="290" alt=" " style="margin-top: 5%"></a>
                    </div>
                    <div class="gap"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="gap"></div>

    <div id="speakers_menu" style="background-color:#fff;padding-top:50px">
        <div class="speakers wow fadeIn ">
            <div class="first_line">
            </div>
            <div class="first_title">
                <p>
                    Upstarter October Attendees
                </p>
            </div>
            <div class="mini_gap">
            </div>
            <div class="speakers_inside_bg">
                <div id="speakers" class="photo_carousel carousel slide wow fadeInUp">
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="https://accenture.com" target="_blank">
                                <img src="images/accenture_logo_black.png" alt="img09"/>
                            </a>
                        </figure>
                    </div>
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="http://zendesk.com" target="_blank">
                                <img class="big-logo" src="images/zen_logo1.svg" alt="img10" style="width:80%"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://deloitte.com" target="_blank">
                                <img src="images/d_logo_black.png" alt="img09"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://wipro.com" target="_blank">
                                <img src="images/wp_logo.png" alt="img10"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://indeed.com" target="_blank">
                                <img class="big-logo" src="images/indeed_logo.png" alt="img09" />
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://kpmg.com" target="_blank">
                                <img src="images/kpmg_logo.png" alt="img10"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://viasat.com" target="_blank">
                                <img src="images/vs_logo.png" alt="img09"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://websummit.com" target="_blank">
                                <img src="images/ws_logo_black.png" alt="img09"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://amazon.com" target="_blank">
                                <img src="images/amazon_logo.png" alt="img09"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://pwc.com" target="_blank">
                                <img src="images/pw_logo1.png" alt="img09"/>
                            </a>
                        </figure>
                    </div>
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="http://ey.com" target="_blank" style="width:70%">
                                <img src="images/ey_logo.png" alt="img09"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://fticonsulting.com" target="_blank">
                                <img src="images/fti_logo.png" alt="img10"/>
                            </a>
                        </figure>
                    </div>
                    <div class="photo_carousel_container">
                    </div>
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="http://learnupon.com" target="_blank">
                                <img src="images/learnupon_logo.png" alt="img09"/>
                            </a>
                        </figure>
                    </div>

                    <div class="photo_carousel_container">
                    </div>
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="http://qualtrics.com" target="_blank">
                                <img src="images/qualtrics_logo_black.png" alt="img09"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://ryanair.com" target="_blank">
                                <img src="images/ryanair_logo.png" alt="img10"/>
                            </a>
                        </figure>
                    </div>
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="http://softwareplacements.ie" target="_blank">
                                <img src="images/software_placements_logo.jpg" alt="img09"/>
                            </a>
                        </figure>
                    </div>
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="http://travelportdigital.com" target="_blank">
                                <img src="images/td_logo.png" alt="img10"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://voxprogroup.com" target="_blank">
                                <img src="images/vp_logo.png" alt="img10"/>
                            </a>
                        </figure>
                    </div>
                    <div class="photo_carousel_container">
                    </div>
                    <div class="photo_carousel_container">
                        <figure class="eventime_img"><a href="http://myfuturenow.co.uk" target="_blank">
                                <img src="images/mfn_logo_black.png" alt="img10"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://solas.com" target="_blank">
                                <img src="images/solas_logo.jpg" alt="img10"/>
                            </a>
                        </figure>
                        <figure class="eventime_img"><a href="http://newrelic.com" target="_blank">
                                <img src="images/nr_logo.svg" alt="img09"/>
                            </a>
                        </figure>
                        <!-- Carousel nav -->
                    </div>
                </div>

                <div class="gap"></div>

                <!-- End About -->
                <div id="speakers_menu" style="background-color:#fff;padding-top:50px">
                    <div class="speakers wow fadeIn ">
                        <div class="first_line">
                        </div>
                        <div class="first_title">
                            <p>
                                Previous Attendees
                            </p>
                        </div>
                        <div class="mini_gap">
                        </div>
                        <div class="speakers_inside_bg">
                            <div id="speakers" class="photo_carousel carousel slide wow fadeInUp">
                                <!-- Indicators -->
                                <!-- Wrapper for Slides -->
                                <div class="photo_carousel_container">
                                    <!-- <figure class="eventime_img"><a href="http://accenture.ie" target="_blank">
                                      <img src="images/accenture_logo1.png" alt="img09"/>
                                    </a>
                                  </figure> -->
                                    <figure class="eventime_img"><a href="https://aerlingus.com" target="_blank">
                                            <img src="images/al_logo1.png" alt="img09"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://zendesk.com" target="_blank">
                                            <img class="big-logo" src="images/zen_logo1.svg" alt="img10" style="width:80%"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://ding.com" target="_blank">
                                            <img src="images/ding_logo_blue_pink1.jpg" alt="img09"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://pwc.com" target="_blank">
                                            <img src="images/pw_logo1.png" alt="img09"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://carguru.com" target="_blank">
                                            <img src="images/carguru_logo1.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://ey.com" target="_blank">
                                            <img class="big-logo" src="images/ey_logo1.png" alt="img09" style="width:70%"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://teamwork.com" target="_blank">
                                            <img src="images/tw_logo1.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://squarespace.com" target="_blank">
                                            <img src="images/ss_logo1.png" alt="img09"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://wrike.com" target="_blank">
                                            <img src="images/wr_logo1.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>

                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://hubspot.com" target="_blank">
                                            <img src="images/hubspot-logo.png" alt="img09"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://learnupon.com" target="_blank">
                                            <img src="images/lu_logo.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://computerfutures.com" target="_blank">
                                            <img src="images/cf_logo.png" alt="img09"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://coupa.com" target="_blank">
                                            <img src="images/coupa_logo1.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://datahug.com" target="_blank">
                                            <img src="images/dh_logo1.png" alt="img09"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://aib.ie" target="_blank">
                                            <img class="big-logo" src="images/aib-logo1.jpg" alt="img10" style="width:70%"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://slack.com" target="_blank">
                                            <img src="images/slack_logo1.jpg" alt="img10"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://gocar.ie" target="_blank">
                                            <img src="images/gocar_logo1.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://masonalexander.ie" target="_blank">
                                            <img src="images/ma_logo1.png" alt="img09"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://showtimeanalytics.com" target="_blank">
                                            <img src="images/showtime_logo.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://lincoln.ie" target="_blank">
                                            <img src="images/lrs_logo.jpg" alt="img09"/>
                                        </a>
                                    </figure>
                                    <figure class="eventime_img"><a href="http://bizimply.com" target="_blank">
                                            <img src="images/bizimply-logo.png" alt="img10"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://food.cloud" target="_blank">
                                            <img class="big-logo" src="images/foodcloud1.png" alt="img09"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://housemydog.com" target="_blank">
                                            <img class="big-logo" src="images/housemydog_logo1.png" alt="img09"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="photo_carousel_container">
                                    <figure class="eventime_img"><a href="http://jobbio.com" target="_blank">
                                            <img class="big-logo" src="images/jobbio_logo2.png" alt="img09"/>
                                        </a>
                                    </figure>
                                </div>
                                <!-- Carousel nav -->
                            </div>
                            <div class="gap">
                            </div>
                        </div>
                        <!-- End Speakers -->

                        <!-- Modal HTML -->
                        <div id="myModal" class="modal fade eventime_modal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times; </button>
                                        <h4 class="modal-title">
                                            Jeremy Crackevich </h4>
                                        <small>
                                            Featured guest </small>
                                    </div>
                                    <div class="modal-body">
                                        <img src="images/featured_speaker.jpg" alt="img10"/>
                                        <p>
                                            <br>
                                            Crackevich's influence on contemporary graphic design is unparalleled. <br>
                                            This outstanding expert and self-named typomaniac is responsible for numerous custom and redesigned typefaces and font identities for world-renowned corporations. Moreover, he has created dozens of commercial typefaces which are considered to be modern classics of typography. <br>
                                            In 1979, he founded CrackevichDesign - German mega-firm providing global design consultancy. The success of this company can be measured by a long list of the most relevant prizes including multiple red dot awards and European Design Awards. <br>
                                            <br>
                                            More about Jeremy Crackevich can be found <a href="#">here</a>.
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="https://www.facebook.com/upstarterdublin/" class="contact_icon fa fa-facebook graybtn graytxt">
                                        </a>
                                        <a href="https://twitter.com/upstarterhq" class="contact_icon fa fa-twitter graybtn graytxt">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- End Past -->
                        <!-- Modal Images -->
                        <div id="imgmodal_1" class="modal fade eventime_modal">
                            <div class="modal-dialog img-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times; </button>
                                        <h4 class="modal-title">
                                            Design expierience </h4>
                                    </div>
                                    <div class="modal-body">
                                        <img class="img_full" src="images/1.jpg" alt="img10"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Contact -->
                        <div id="about_menu" class="wow fadeIn">
                            <div class="contact ">
                                <div class="mask">
                                    <div class="first_line">
                                    </div>
                                    <div class="first_title">
                                        <p>
                                            Contact The Team
                                        </p>
                                    </div>
                                    <!-- <div class="first_description">
                                      <p>
                                        UpStarter provides candidates with a chance to meet some of Ireland's most innovative companies who are all hiring. Employers on the night will be looking for developers, designers, marketers and students looking for internships.

                              If, on the other hand, you are on the lookout for a co-founder for your own startup, UpStarter will also provide you with a platform to meet other like minded people in a casual setting.
                                      </p>
                                    </div> -->
                                    <form id="contact_menu" class="eventime_contact_form" method="POST" action="http://formspree.io/info@upstarter.ie">
                                        <input type="hidden">
                                        <input name="first_name" class="" type="text" placeholder="First Name" required>
                                        <input name="last_name" class="rightinput" type="text" placeholder="Last Name" required>
                                        <input name="email"  type="email" placeholder="Email" required>
                                        <input name="telephone"  class="rightinput" type="tel" placeholder="Phone" required>
                                        <textarea placeholder="Message" name="message" class="make_a_reservation_message" required></textarea>
                                        <input type="submit" value="send message" class="contact_button">
                                        <div class="clear">
                                        </div>
                                        <div id="contactMsgc">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <footer>
                            <div class="footer wow fadeIn">
                                <div class="mini_gap">
                                </div>
                            </div>
                        </footer>
                        <div class="footer_second">

                            <div class="footer_second_left">
                                <p class="copyright"><span>&copy;</span>Upstarter</p>

                            </div>
                            <div class="footer_second_right">
                                <a href="https://www.facebook.com/upstarterdublin/" class="footer_icon fa fa-facebook">
                                </a>
                                <a href="https://twitter.com/upstarterhq" class="footer_icon fa fa-twitter">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

