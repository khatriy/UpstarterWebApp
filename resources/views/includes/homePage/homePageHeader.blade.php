<nav class="navbar navbar-fixed-top nav_mask">
    <div class="navi_container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">
    Toggle navigation </span>
                <span class="icon-bar">
    </span>
                <span class="icon-bar">
    </span>
                <span class="icon-bar">
    </span>
            </button>
            <a class="navbar-brand" href="#home_slider">
                <img src="/images/upstarter-logo-white.png" alt="img09"/>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul style="display: inline-block" class="nav navbar-nav">
                <li>
                    <a href="#about_menu">
                        About </a>
                </li>
                <li>
                    <a href="{{route('companies')}}">
                        Companies </a>
                </li>
                @guest
                <li><a href="{{ route('login') }}">Login</a></li>
                @else
                <li class="dropdown">
                    <a href="{{route('login')}}"  >
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a style="background-color: white" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endguest
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.nav-collapse -->
</nav>