
    <!-- Scripts -->
    <script type="text/javascript" src="{{asset('js/jquery-2.2.1.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/minify.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuex/2.3.1/vuex.js"></script>
    <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.cd-testimonials-wrapper').flexslider({
                selector: ".cd-testimonials > li",
                animation: "slide",
                controlNav: true,
                slideshow: true,
                smoothHeight: true,
                directionNav: false,
                start: function() {
                    $('.cd-testimonials').children('li').css({
                        'opacity': 1,
                        'position': 'relative'
                    });
                }
            });
        });
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
