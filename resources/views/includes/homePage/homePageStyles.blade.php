
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Ireland's Technology Recruitment Event"/>
    <meta name="author" content="">
    <title>
        UpStarter </title>
    <link rel='shortcut icon' type='image/x-icon' href="{{asset('favicon.ico')}}"/>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/companyCardCss.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Google Font Raleway -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,700,300' rel='stylesheet' type='text/css'>
    <!-- Font Awsome Icons CSS -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- Colors and Backgrounds CSS -->
    <link href="{{asset('css/colors_5.css')}}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
