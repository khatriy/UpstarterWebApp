<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Companies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies',function (Blueprint $table){
           $table->increments('cmpId');
           $table->integer('userId')->unique();
           $table->string('cmpName');
           $table->integer('cmpStatus')->default(0);
           $table->string('cmpRegNumber');
           $table->string('cmpNatureOfBusiness');
           $table->string('cmpVatNumber');
           $table->string('cmpAddress');
           $table->string('cmpLogo');
           $table->string('cmpBillingAddress');
           $table->string('telephone');
           $table->string('cmpEmail');
           $table->string('cmpAccPayTelephone');
           $table->string('cmpAccPayEmail');
           $table->string('cmpRepName');
           $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.  `
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
